<?php  
include("dbcon.php");
function dateDiffInDays($cl_d, $c_date)  
{ 
    // Calulating the difference in timestamps 
    $diff = strtotime($cl_d) - strtotime($c_date); 
      
    // 1 day = 24 hours 
    // 24 * 60 * 60 = 86400 seconds 
    return abs(round($diff / 86400)); 
}

$requestreg = $_GET['value'];
$data = array();
$paidamount = 0;
$totalamnt = 0;
$regno = mysqli_query($con,"SELECT * FROM clients WHERE registration_no = '$requestreg'");
$pymtdtls = mysqli_query($con,"SELECT * FROM payment_details WHERE customer_regno = '$requestreg'");
while($regd = mysqli_fetch_array($regno)){
	$data['name'] =  $regd['name'];
	$data['id'] =  $regd['id'];
	$data['o_date'] =  $regd['open_date'];
	$data['c_date'] =  $regd['close_date'];
	$data['intr'] =  $regd['interest'];
	$data['damnt'] =  $regd['debit_amount'];
	$totalamnt = $regd['total_amount'];
	$data['total_amount'] =  $totalamnt;
	$cl_d = $regd['close_date'];
}
while ($paydtl = mysqli_fetch_array($pymtdtls)) {
	$paidamount +=  $paydtl['collected_amount'];
}
$c_date = Date("Y-m-d");
$dateDiff = dateDiffInDays($cl_d, $c_date);
$data['amt_paid'] = $paidamount;
$data['balance'] = $totalamnt - $paidamount;
$data['r_days'] = $dateDiff;
if($cl_d <= $c_date)
{
	$data['block'] = '0';
}else{
	$data['block'] = '1';
}
echo json_encode($data);
?>