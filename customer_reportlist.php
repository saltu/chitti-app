<?php include('header.php') ;
include("dbfiles/dbcon.php");

// to find days between two days
function dateDiffInDays($cl_d, $c_date)  
{ 
    // Calulating the difference in timestamps 
    $diff = strtotime($cl_d) - strtotime($c_date); 
      
    // 1 day = 24 hours 
    // 24 * 60 * 60 = 86400 seconds 
    return abs(round($diff / 86400)); 
}

$regno = $_GET['rregno'];
$name = $_GET['custname'];
$top = mysqli_query($con,"SELECT * FROM clients WHERE registration_no = '$regno'");
$detail = mysqli_fetch_array($top);
$s_top = mysqli_query($con,"SELECT * FROM security_person WHERE client_id = '$regno'");
$details = mysqli_fetch_assoc($s_top);
$cl_d =$detail[13];
$c_date = Date("Y-m-d");
$dateDiff = dateDiffInDays($cl_d, $c_date);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Customer Report</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE TOOLBAR -->
    </div>
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="home.php">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Customer Report</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-settings font-red"></i>
                                <span class="caption-subject font-red sbold uppercase"><?php echo $name ?>&nbsp&nbsp&nbsp Report</span>
                                <span style="margin-left: 700px" class="caption-subject font-red sbold uppercase">Days Remaining <?php echo $dateDiff ?></span>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"> </a>
                                <a href="javascript:;" class="reload"> </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="form-body">
                            <h3 class="form-section">Details</h3>
                                <div class="row">                
                                    <!-- Name -->
                                    <!-- <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Customer Name</label>
                                            <div class="col-md-9">
                                                <input type="text" readonly class="form-control custname" id="custname" name="custname" value="<?php echo $detail[2]; ?>">
                                            </div>
                                        </div>
                                    </div> -->
                                    <!-- Bill number automatic -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Register No</label>
                                            <div class="col-md-9">
                                                <input type="text" readonly class="form-control registerno" id="registerno" name="registerno" value="<?php echo $detail[1]; ?>">
                                            </div>
                                        </div>
                                    </div>        
                                    <!-- phone number  -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Payment Type</label>
                                            <div class="col-md-9">
                                                <input type="text" readonly class="form-control pay_type" id="pay_type" name="pay_type" value="<?php echo $detail[9]; ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                        <label class="control-label col-md-3">Mobile No.</label>
                                            <div class="col-md-9">
                                                <input type="text" readonly value="<?php echo $detail[7]; ?>" class="form-control mobile" id="mobile" name="mobile">
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <!--row-->                                            
                                <br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                        <label class="control-label col-md-3">Interest(% / Amount)</label>
                                            <div class="col-md-9">
                                                <input type="number" readonly class="form-control interest" id="interest" name="interest" value="<?php echo $detail[14]; ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                        <label class="control-label col-md-3">Description</label>
                                            <div class="col-md-9">
                                                <textarea rows="3" cols="10" readonly class="form-control desc" id="desc" name="desc"><?php echo $detail[3]; ?> </textarea>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <div class="form-group">
                                        <label class="control-label col-md-3">Landline</label>
                                            <div class="col-md-9">
                                                <input type="text" readonly class="form-control landline" id="landline" name="landline" value="<?php echo $detail[8]; ?>">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <!-- row -->
                                <br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Place</label>
                                            <div class="col-md-9">
                                                <input type="text" readonly class="form-control place" id="place" value="<?php echo $detail[4]; ?>" name="place">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                        <label class="control-label col-md-3">Address</label>
                                            <div class="col-md-9">
                                                <textarea rows="3" cols="10" readonly class="form-control address" id="address" name="address"><?php echo $detail[5]; ?> </textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                        <label class="control-label col-md-3">PIN</label>
                                            <div class="col-md-9">
                                                <input type="text" readonly class="form-control pin" id="pin" name="pin" value="<?php echo $detail[6]; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <!-- row -->
                                <div class="row">
                                    <h3>Security Person Details</h3>
                                    <br>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                        <label class="control-label col-md-3">Name</label>
                                            <div class="col-md-9">
                                                <input type="text" readonly class="form-control s_name" id="s_name" name="s_name" value="<?php echo $details["name"]; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                        <label class="control-label col-md-3">Address</label>
                                            <div class="col-md-9">
                                                <textarea rows="3" cols="10" readonly class="form-control s_address" id="s_address" name="s_address"><?php echo $details["address"]; ?> </textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                        <label class="control-label col-md-3">Phone</label>
                                            <div class="col-md-9">
                                                <input type="text" readonly class="form-control pin" id="pin" name="pin" value="<?php echo $details["phone"]; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                </div>                                  
                            </div>
                            <br>
                            <table class="table table-striped table-bordered table-hover" id="sample_1">
                                <thead>
                                    <tr>
                                        <th> Sl.No </th>
                                        <th> Debited Amount </th> 
                                        <th> Opening Date </th>
                                        <th> Closing Date </th>
                                        <th> Paid amount </th>
                                        <th> Interest Amount </th>
                                        <th> Balance Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1;
                                    $tot_debit = 0;
                                    $tot_collected = 0; 
                                    $sql = mysqli_query($con,"SELECT * FROM clients WHERE registration_no = '$regno'");
                                    while ($result = mysqli_fetch_array($sql)) {
                                    ?>
                                            <tr id="<?php $result[0]; ?>">
                                                <td class="text-center"> <?php echo $i; ?> </td>
                                                <td class="text-center"> <?php echo $result[10]; ?> </td>
                                                <td class="text-center"> <?php echo $result[12]; ?> </td>
                                                <td class="text-center"> <?php echo $result[13]; ?> </td>
                                                <?php
                                                $totalpaid = 0;
                                                $sql1 = mysqli_query($con,"SELECT * FROM payment_details where customer_regno = '$result[1]'");
                                                while($pay_details = mysqli_fetch_array($sql1)){
                                                    $totalpaid += $pay_details[3];
                                                }
                                                ?>
                                                <td class="text-center"> <?php echo $totalpaid; ?> </td>
                                                <?php 
                                                $tinterest = 0;
                                                $sql2 = mysqli_query($con,"SELECT interest_amount FROM interest_details WHERE registration_no = '".$result[1]."' ");
                                                while($interest = mysqli_fetch_array($sql2)){
                                                    $tinterest += $interest[0];
                                                }
                                                 ?>
                                                <td class="text-center"> <?php echo $tinterest; ?> </td>
                                                <?php 
                                                $balanceamt = (($result[10] + $tinterest) - $totalpaid);
                                                ?>
                                                <td class="text-center"> <?php echo $balanceamt; ?></td>
                                            </tr>
                                            <tr class="warning">
                                                <td colspan="2" class="text-center">Date</td>
                                                <td colspan="2" class="text-center">Interest Amount Payed</td>
                                                <td colspan="2" class="text-center">Loan Amount Payed</td>
                                                <td colspan="2" class="text-center">Total Payed Amount</td>
                                                <th> Edit </th>
                                            </tr>
                                    <?php
                                        $i++;
                                    }
                                    $tot_collected = 0;
                                    $intr1 = 0;
                                    $sql3 = mysqli_query($con,"SELECT * FROM payment_details WHERE customer_regno = '$regno' AND collected_amount != 0 ORDER BY pdate DESC");
                                    while ($result1 = mysqli_fetch_array($sql3)) {
                                    ?>
<!--                                             <tr>
                                                <td colspan="4" class="text-center"> <?php echo $result1[1]; ?> </td>
                                                <td colspan="3" class="text-center"> <?php echo $result1[4]; $intr1 += $result1[4] ?> </td>
                                                <td colspan="4" class="text-left"><?php echo $result1[3]; ?></td>

                                        $intr1 = 0;
                                         $intr_tot += $result1[4];
                                    ?> -->
                                            <tr>
                                                <td colspan="2" class="text-center"> <?php echo $result1[1]; ?> </td>
                                                <td colspan="2" class="text-center"> <?php echo $result1[4]; ?> </td>
                                                <td colspan="2" class="text-center"> <?php echo $result1[5]; ?> </td>
                                                <td  colspan="2" class="text-center"><?php echo $result1[3]; ?></td>
                                                <td class="text-center"><a class="editveh" href="payment_edit.php?id=<?php echo $result1[0];?>"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                                                <?php $tot_collected +=$result1[3]; ?>
                                            </tr>
                                    <?php
                                        $i++;
                                        }
                                        if ( $detail[9] == "daily" ) {
                                    ?>
                                    <tr>
                                        <td colspan="4" class="text-right"><b>Total Extended Days</b></td>
                                        <td colspan="5"><?php echo $detail[17]; ?></td>
                                    </tr>
                                    <?php } ?>
                                    <tr>
                                        <td colspan="4" class="text-right"><b>Total Interest Payed</b></td>
                                        <td colspan="5"><?php echo $intr1; ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="text-right"><b>Total Payed(Inc. Interest)</b></td>
                                        <td colspan="5"><?php echo $tot_collected; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>                
            </div>
        </div>
    </div>
<!-- END PAGE CONTENT INNER -->
</div>
<!-- END PAGE CONTENT BODY -->
<?php include('footer.php') ?>