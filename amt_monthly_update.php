<?php include('header.php') ;
include("dbfiles/dbcon.php");
$mregno = $_GET['mregno'];
$c_date = date('Y-m-d');
$dpay = mysqli_query($con,"SELECT * FROM payment_details WHERE customer_regno = '$mregno' AND pdate = '$c_date'");
if (mysqli_num_rows($dpay) != 0) {
   echo "<h3><center>Already payed on this day</center></h3>";
   echo "<center><a href='daily_amt.php'><button type='button' class='btn btn-circle blue'>Go Back</button></a></center> ";
   die();
}
?>
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Monthly Client</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="home.php">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Customer Monthly Update</a>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->            
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>
                                    </div>
                                    <!-- <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="javascript:;" class="reload"> </a>
                                    </div> -->
                                </div>
                                <div class="portlet-body">
                                    
                                    <div class="row">                                    
                                        <div class="alert alert-danger display-hide" id="alert">
                                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                        </div>
                                        <div class="alert alert-success display-hide" id="alert2">
                                            <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                        </div>
                                    </div>
                                    <!-- BEGIN FORM-->
                                   <form action="#" method="POST" class="form-horizontal" id="monthlyc">
                                        <div class="form-body">
                                            <h3 class="form-section">Info</h3>
                                            <div class="row">
                                            <div class="col-md-8">
                                                <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Date</label>
                                                        <div class="col-md-8">
                                                            <?php 
                                                                $month = date('m');
                                                                $day = date('d');
                                                                $year = date('Y');

                                                                $today = $year . '-' . $month . '-' . $day;
                                                            ?>
                                                            <input type="date" min="<?php echo $today; ?>" value="<?php echo $today; ?>" class="form-control" id="cudate" name="cudate" required placeholder="Date">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">

                                                <!-- Bill number automatic -->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Registration Number</label>
                                                        <div class="col-md-8">
                                                            <input type="text" required class="form-control mregno" id="mregno" name="mregno" value="<?php echo $mregno ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <!-- Name -->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Name</label>
                                                        <div class="col-md-8">
                                                            <input type="text" required readonly class="form-control custname" id="custname" name="custname" placeholder="Customer name">
                                                            <input type="hidden" id="customer_id" name="customer_id" class="form-control form-filter input-sm customer_id" value="0" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <!-- phone number  -->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Opening Date</label>
                                                        <div class="col-md-8">
                                                            <input type="date" readonly class="form-control open_date" id="open_date" name="open_date">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <!-- phone number  -->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Closing Date</label>
                                                        <div class="col-md-8">
                                                            <input type="date" readonly value="0" class="form-control close_date" id="close_date" name="close_date">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="amt_mtable" class="col-md-4">
                                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th> Sl.No </th>
                                                <th> Date </th>
                                                <th> Interest </th>
                                                <th> Amount </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; 
                                            $sql = mysqli_query($con,"SELECT * FROM payment_details WHERE customer_regno = '$mregno' AND collected_amount != 0 ORDER BY pdate DESC");
                                            while ($results = mysqli_fetch_assoc($sql)) {
                                                                            ?>
                                                <tr id="<?php $results['id']; ?>">
                                                    <td class="text-center"> <?php echo $i; ?> </td>
                                                    <td class="text-center"> <?php echo $results['pdate']; ?>
                                                    </td>
                                                    <td class="text-center"> <?php echo $results['interest_amount']; ?> </td>
                                                    <td class="text-center"> <?php echo $results['loan_amount']; ?> </td>
                                            <?php
                                                $i++;
                                            }
                                            ?>
                                        </tbody>
                                        </table>
                                            
                                        </div>
                                        </div>
                                            <!--/row-->
                                            <div class="row">
                                            <h3 class="form-section col-md-7">Details</h3>
                                            <h3 class="form-section col-md-4">Months Remaining : <span class="r_days" id="r_days"></span></h3>
                                            </div>
                                            <!--/row-->
                                            <div class="row" >
                                                <div class="col-md-12">
                                                    <div class="portlet-body">
                                                        <div class="table-responsive" style="height:150px !important;" >
                                                            <table class="table table-striped table-bordered table-hover">
                                                                <thead>
                                                                    <tr class="d-flex" >
                                                                        <th class ="text-center col-md-1"> Loan Amount</th>
                                                                        <th class ="text-center col-md-1"> Interest % </th>
                                                                        <th class ="text-center col-md-1"> Current Loan Amount </th>
                                                                        <th class ="text-center col-md-1"> Current Interest Amount </th>
                                                                        <!-- <th class ="text-center col-md-2"> This Month Interest Paid </th> -->
                                                                        <th class ="text-center col-md-2"> Balance Interest Amount of This month </th>
                                                                        <th class ="text-center col-md-1"> Total Interest Paid </th>
                                                                        <th class ="text-center col-md-1"> Total loan Amount Paid </th>
                                                                        <th class="text-center col-md-2">Interest Amount</th>
                                                                        <th class="text-center col-md-2"> Today's Payment </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                   <tr id="sales-table">
                                                                        <td><input required type="number" id="debit_amount" name="debit_amount" readonly class="form-control form-filter input-sm debit_amount">
                                                                        </td>

                                                                        <td><input required type="number" id="intr" name="intr" readonly class="form-control form-filter input-sm intr" value="0">
                                                                        </td>

                                                                        <td><input required type="number" id="loa_cu" name="loa_cu" readonly class="form-control form-filter input-sm loa_cu" value="0">
                                                                        </td>

                                                                        <td><input type="number" id="c_int" name="c_int" readonly class="form-control form-filter input-sm c_int">
                                                                            <input type="hidden" id="tot_amt" name="tot_amt" readonly class="form-control form-filter input-sm tot_amt">
                                                                        </td>

                                                                        <input required type="hidden" id="total_interest" name="total_interest" readonly class="form-control form-filter input-sm total_interest" value="0">

                                                                        <td>
                                                                        <input required type="number" id="dint_amnt" name="dint_amnt" readonly class="form-control form-filter input-sm dint_amnt" value="0">
                                                                        </td>

                                                                        <td><input required type="number" id="tintr" name="tintr" readonly class="form-control form-filter input-sm tintr" value="0">
                                                                        </td>

                                                                        <td><input required type="number" id="amt_paid" name="amt_paid" readonly class="form-control form-filter input-sm amt_paid" value="0">
                                                                        </td>

                                                                        <td> <input required type="number" class="form-control form-filter input-sm intr_amt" id="intr_amt" name="intr_amt"> </td>  

                                                                        <td> <input required type="number" class="form-control form-filter input-sm mtdy_pymt" id="mtdy_pymt" name="mtdy_pymt"> </td>

                                                                </tbody>
                                                            </table>        
                                                        </div>                                           
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                        </div>
                                        <center>
                                        <div class="form-actions display-hide" id="for_mes">
                                            <center><p style="color: red;">Entered interest amount is greater than current month interest</p></center>
                                        </div>
                                        <div class="form-actions" id="for_intval">
                                            <button type="submit" name="submit" id="submitsalesbil" class="btn green ">Submit</button>
                                            <button type="button" id="cancelsalesbil" class="btn default ">Cancel</button>

                                            <div class="row">                                    
                                                <div class="alert alert-danger display-hide" id="alertsalespay">
                                                    <button class="close" data-close="alert"></button> The Amount you entered is higher. 
                                                </div>
                                            </div>
                                        </div>
                                        </center>
                                    </form>
                                    <?php
                                    if(isset($_POST["submit"])){

                                        $inter_am = 0;
                                        $loa_am = 0;

                                        $inter = ($_POST['loa_cu']*$_POST['intr'])/100;
                                        // $total_interest = $_POST['total_interest'];

                                        // if($inter == $total_interest){
                                        //     $inter_am = 0;
                                        //     $loa_am = $_POST['mtdy_pymt'];
                                        // } else if($inter > $total_interest ) {
                                        //     $ba_in = $inter - $total_interest;
                                        //     if($ba_in < $_POST['mtdy_pymt']){
                                        //         $inter_am = $ba_in;
                                        //         $loa_am = $_POST['mtdy_pymt'] - $ba_in;
                                        //     } else if($ba_in > $_POST['mtdy_pymt']){
                                        //         $inter_am = $_POST['mtdy_pymt'];
                                        //         $loa_am = 0;
                                        //     }
                                        // }
                                        // $b_int = $inter - $inter_am;

                                        $loa_am = $_POST['mtdy_pymt'];
                                        $total_interest = $_POST['intr_amt'];
                                        if($inter == $total_interest){
                                            $inter_am = $_POST['intr_amt'];
                                            $b_int = $inter - $inter_am;
                                        } else if($inter > $total_interest ) {
                                            $inter_am = $_POST['intr_amt'];
                                            $b_int = $inter - $inter_am;
                                        } else if($inter < $total_interest){
                                            $inter_am = $_POST['intr_amt'];
                                            $b_int = 0;
                                        } else{
                                        $b_int = 0;
                                        }
                                        $cltd_amt = $loa_am + $inter_am;


                                        // $tdpay = "INSERT INTO payment_details(pdate,customer_regno,collected_amount,interest_amount,loan_amount,balance_amount,mcheck_status) VALUES('".$_POST['cudate']."','".$_POST['mregno']."','".$_POST['mtdy_pymt']."','".$inter_am."','".$loa_am."','0','0')";
                                        // $insert = mysqli_query($con,$tdpay);

                                        $tdpay = "INSERT INTO payment_details(pdate,customer_regno,collected_amount,interest_amount,loan_amount,balance_amount,mcheck_status) VALUES('".$_POST['cudate']."','".$_POST['mregno']."','".$cltd_amt."','".$inter_am."','".$loa_am."','0','0')";
                                        $insert = mysqli_query($con,$tdpay);


                                        // if($loa_am != 0){
                                        // $query1 = "UPDATE clients SET debit_amount -='".$loa_am."' WHERE registration_no ='".$_POST['mregno']."'";
                                        // $insert2 = mysqli_query($con,$query1);
                                        // }

                                        $query = "INSERT INTO interest_details(registration_no,interest_amount,cdate) VALUES('".$_POST['mregno']."','".$inter_am."','".$_POST['cudate']."')";
                                        $insert1 = mysqli_query($con,$query);

                                        $query1 = "UPDATE minterest_details SET balinterest_amount ='$b_int' WHERE registration_no ='".$_POST['mregno']."'";
                                        $insert2 = mysqli_query($con,$query1);

                                        if (!$insert || !$insert1 ) {
                                        printf("Error: %s\n", mysqli_error($con));
                                        exit();
                                        }

                                        // if ($_POST['pint_amnt'] != 0) {
                                        //     $query = "INSERT INTO interest_details(registration_no,interest_amount,cdate) VALUES('".$_POST['mregno']."','".$_POST['pint_amnt']."','".$_POST['cudate']."')";
                                        //     $insert1 = mysqli_query($con,$query);
                                        // }

                                        if($insert == 1){
                                            echo "<script>alert('Amount added')</script>";
                                            echo "<script>window.location='daily_amt.php'</script>";
                                        }
                                        else{
                                            echo "<script>alert('You have some form errors. Please check below')</script>";
                                            header("Location: amt_monthly_update.php"); 
                                        }

                                    }
                                    ?>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include('footer.php') ?>
<script type="text/javascript">
//     var y = new Date();
//     var m = y.getMonth();
//     var yr = y.getFullYear();
//     var x =daysInMonth(++m, yr);
//     function daysInMonth(month, year) {
//     return new Date(year, month, 0).getDate();
// }
    $(document).ready(function(){
    var value = $("#mregno").val();
    $.ajax({
            type:'get',
            url:'dbfiles/mpaymentdtls.php',
            data:{value:value},
            success:function(data)
            {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $('#custname').val(json['name']);
                $('#customer_id').val(json['id']);
                $('#open_date').val(json['o_date']);
                $('#close_date').val(json['c_date']);
                $('#debit_amount').val(json['debit_amount']);
                $('#intr').val(json['intr']);
                $('#loa_cu').val(json['debit_loan']);
                var inter_amount = Math.round(json['cm_intr']);
                $('#tot_amt').val(inter_amount);
                $('#c_int').val(json['c_int']);
                $('#total_interest').val(Math.round(json['total_intr']));
                $('#tintr').val(Math.round(json['tintr']));
                var bal_inte = inter_amount - json['total_intr'];        
                $('#dint_amnt').val(Math.round(bal_inte));
                $('#amt_paid').val(Math.round(json['amt_paid']));
                $('#r_days').html(json['r_days']);
                if (json['block'] == '0' ) {
                    $('#monthlyc').html("Closing Date Exceeded <a style='margin-left: 30%;' href='daily_amt.php'><button type='button' class='btn btn-circle blue'>Cancel</button></a>");
                }
            }
    })
    $("#intr_amt").focus();
});
</script>