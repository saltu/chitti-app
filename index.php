<!DOCTYPE html>
<html lang="en">

<head>
  <title>Login</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!--===============================================================================================-->
  <link rel="icon" type="image/png" href="login_assets/images/icons/favicon.ico" />
  <link rel="stylesheet" type="text/css" href="login_assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="login_assets/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
  <link rel="stylesheet" type="text/css" href="login_assets/vendor/animate/animate.css">
  <link rel="stylesheet" type="text/css" href="login_assets/vendor/css-hamburgers/hamburgers.min.css">
  <link rel="stylesheet" type="text/css" href="login_assets/vendor/animsition/css/animsition.min.css">
  <link rel="stylesheet" type="text/css" href="login_assets/vendor/select2/select2.min.css">
  <link rel="stylesheet" type="text/css" href="login_assets/vendor/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" type="text/css" href="login_assets/css/util.css">
  <link rel="stylesheet" type="text/css" href="login_assets/css/main.css">
  <!--===============================================================================================-->
</head>

<body>
  <?php
    include("dbfiles/dbcon.php");
   session_start();
   if(!empty($_SESSION['username'])){
    header("Location:home.php");
   }
   ?>
  <?php
  $date = date('Y-m-d');
  $month = date('m');
  // echo $month;
  $pre_month = date('m', strtotime(date('Y-m')." -1 month"));
  // First day of the month.
  $firt_date = date('Y-m-01', strtotime($date));
  // $firt_date = date('Y-m-d');
  // if($firt_date == $date){
    $pymtdtls = mysqli_query($con,"SELECT * FROM payment_details WHERE Month(pdate) = '$pre_month'AND mcheck_status = '0'");
    while ($paydtl = mysqli_fetch_array($pymtdtls)) {
      $query1 = "UPDATE clients SET debit_loan = debit_loan -'".$paydtl['loan_amount']."' WHERE registration_no = '".$paydtl['customer_regno']."' ";
      $query2 = "UPDATE payment_details SET mcheck_status = '1' WHERE customer_regno = '".$paydtl['customer_regno']."' ";
      mysqli_query($con,$query2) or die(mysqli_error($con));
      $insert2 = mysqli_query($con,$query1) or die(mysqli_error($con));
    }    
  // }
  ?>
   <?php
   
   // if($_SERVER["REQUEST_METHOD"] == "POST")
   if(isset($_POST['submit'])) {
      // username and password sent from form 
      
      $myusername =($_POST['username']);
      $mypassword =($_POST['pass']); 
      
      $sql = "SELECT id FROM admin WHERE username = '$myusername' and password = '$mypassword'";
      $result = mysqli_query($con,$sql);
      $row = mysqli_fetch_assoc($result);
      
      $count = mysqli_num_rows($result);
      
      // If result matched $myusername and $mypassword, table row must be 1 row
      $expdate = date('d-m-Y');   

      if($count == 1) {
        if($expdate == '13-11-2020'){
          header("Location:expire.php");
        } else{
          $_SESSION['username'] = $myusername;
          header("Location:home.php");
        }
      }else {
        $_SESSION['error'] = 'Your Login Name or Password is invalid!';
        header("Location:index.php");
      }
   }
?>

  <div class="limiter">
    <div class="container-login100" style="background-image: url('login_assets/images/bg-02.jpg');">
      <div class="wrap-login100 p-t-30 p-b-50">
        <span class="login100-form-title p-b-41"><?php
        if(!empty($_SESSION['error'])){
          echo $_SESSION['error'].'<br>';
        }
        if (!empty($_SESSION['message'])) {
           echo $_SESSION['message'];
         } ?>
            Login
        </span>
        <form class="login100-form validate-form p-b-33 p-t-5" action ="" method ="post">

          <div class="wrap-input100 validate-input" data-validate="Enter username">
            <input class="input100" autocomplete="off" type="text" name="username" placeholder="User name">
            <span class="focus-input100" data-placeholder="&#xe82a;"></span>
          </div>

          <div class="wrap-input100 validate-input" data-validate="Enter password">
            <input class="input100" type="password" name="pass" placeholder="Password">
            <span class="focus-input100" data-placeholder="&#xe80f;"></span>
          </div>

          <div class="container-login100-form-btn m-t-32">
            <button class="login100-form-btn" name="submit">
              Login
            </button>
          </div>

        </form>
      </div>
    </div>
  </div>


  <div id="dropDownSelect1"></div>


  <script src="login_assets/vendor/jquery/jquery-3.2.1.min.js"></script>
  <script src="login_assets/vendor/animsition/js/animsition.min.js"></script>
  <script src="login_assets/vendor/bootstrap/js/popper.js"></script>
  <script src="login_assets/vendor/bootstrap/js/bootstrap.min.js"></script>
  <script src="login_assets/vendor/select2/select2.min.js"></script>
  <script src="login_assets/vendor/daterangepicker/moment.min.js"></script>
  <script src="login_assets/vendor/daterangepicker/daterangepicker.js"></script>
  <script src="login_assets/vendor/countdowntime/countdowntime.js"></script>
  <script src="login_assets/js/main.js"></script>

</body>

</html>