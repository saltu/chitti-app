<?php
$con = mysqli_connect("localhost","root","","chittiapp");
$count = 0;
if (!$con) {
	echo "Failed to connect to Database:";
} else {
	$sql = "SELECT COUNT(DISTINCT `table_name`) FROM `information_schema`.`columns` WHERE `table_schema` = 'chittiapp'";
	$result = mysqli_query($con,$sql);

	$row = mysqli_fetch_row($result);
	$count = $row[0];

	if ($count <= 0) {		

	    // Name of the file
		$filename = 'db_backup/chittiapp.sql';
		// $filename = 'chittiapp.sql';

		// Temporary variable, used to store current query
		$templine = '';
		// Read in entire file
		$lines = file($filename);
		// Loop through each line
		foreach ($lines as $line) {
			// Skip it if it's a comment
			if (substr($line, 0, 2) == '--' || $line == '')
			    continue;

			// Add this line to the current segment
			$templine .= $line;
			// If it has a semicolon at the end, it's the end of the query
			if (substr(trim($line), -1, 1) == ';'){
			    // Perform the query
			    mysqli_query($con, $templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysqli_error($con) . '<br /><br />');
			    // Reset temp variable to empty
			    $templine = '';
			}
		}
	}
	echo '<script type="text/javascript">alert("Backup Restores Successfully");</script>';
    echo '<script>location.href="index.php"</script>';
}