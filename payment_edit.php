<?php include('header.php');
include("dbfiles/dbcon.php");

$id =$_GET['id'];
$sql = mysqli_query($con,"SELECT * FROM payment_details WHERE id = '$id'");
$cdtls = mysqli_fetch_assoc($sql);
    $p_date = $cdtls["pdate"];
    $regno = $cdtls["customer_regno"];
    $clt_amnt = $cdtls["loan_amount"];
    $intr_amount = $cdtls["interest_amount"];
    $collected_amt = $cdtls["collected_amount"];
    $balnc_amt = $cdtls["balance_amount"];
$sql1 = mysqli_query($con,"SELECT payment_type FROM clients WHERE registration_no = '$regno'");
    $idtls = mysqli_fetch_array($sql1);
            $type = $idtls[0];
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Payment details</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE TOOLBAR -->
    </div>
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="home.php">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Edit Payment</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-settings font-red"></i>
                                <span class="caption-subject font-red sbold uppercase">Payment</span>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"> </a>
                                <a href="javascript:;" class="reload"> </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-toolbar" style="margin-bottom: 80px !important;">
                                <div class="row">                                    
                                    <div class="alert alert-danger display-hide" id="alert">
                                        <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                    </div>
                                    <div class="alert alert-success display-hide" id="alert2">
                                        <button class="close" data-close="alert"></button> Client Data is successfully Saved!
                                    </div>
                                </div>
                                <div class="row">                                    
                                    <div class="col-md-12">
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <form action="#" method="POST" class="form-horizontal">            
                                            <div class="row form-body">
                                                <div class="col-md-6">
                                                    <h3><center>Payment Details</center></h3>
                                                    <br>
                                                    <?php if ($type == 'monthly') {
                                                    ?>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Date</label>
                                                        <div class="col-md-8">
                                                            <input type="date" required class="form-control" name="pay_date" id="pay_date" value="<?php echo $p_date; ?>" placeholder="Payment Date">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Loan Amount Payed</label>
                                                        <div class="col-md-8">
                                                            <input type="text" required class="form-control" name="loan_amount" id="loan_amount" value="<?php echo $clt_amnt; ?>" placeholder="Loan Amount">
                                                        </div>
                                                    </div>

                                                    

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Interest Amount Payed</label>
                                                        <div class="col-md-8">
                                                            <input type="number" required class="form-control" name="interest_amount" id="interest_amount" value="<?php echo $intr_amount; ?>" placeholder="Interest Amount">
                                                        </div>
                                                    </div>

                                                <?php }else{ ?>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Date</label>
                                                        <div class="col-md-8">
                                                            <input type="date" required class="form-control" name="pay_date" id="pay_date" value="<?php echo $p_date; ?>" placeholder="Payment Date">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Collected Amount</label>
                                                        <div class="col-md-8">
                                                            <input type="text" required class="form-control" name="loan_amount" id="loan_amount" value="<?php echo $collected_amt; ?>" placeholder="Loan Amount">
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                </div>
                                                </div>
                                                <div class="form-actions" style="border-top:0 !important;">
                                                    <div class="row">
                                                        <div class="col-md-offset-4 col-md-9">
                                                            <button type="submit" name="submit" class="btn btn-circle green">Submit</button>
                                                            <a href="{{ route('home') }}"><button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- END FORM-->
                                            <?php
                                            if(isset($_POST["submit"])){
                                                if ($type == "monthly") {
                                                $total_amt = $_POST['interest_amount'] + $_POST['loan_amount'];
                                                $query = "UPDATE payment_details SET pdate ='".$_POST['pay_date']."',collected_amount ='".$total_amt."',interest_amount ='".$_POST['interest_amount']."',loan_amount ='".$_POST['loan_amount']."' WHERE id = $id";
                                                $insert = mysqli_query($con,$query);
                                                }else{
                                                $query = "UPDATE payment_details SET pdate ='".$_POST['pay_date']."',collected_amount ='".$_POST['loan_amount']."' WHERE id = $id";
                                                $insert = mysqli_query($con,$query);
                                                }
                                                if($insert == 1){
                                                    echo "<script>alert('Client Details Succeffully Updated')</script>";
                                                    echo "<script>window.location='customer_report.php'</script>";
                                                }
                                                else{
                                                    echo "<script>alert('You have some form errors. Please check below'.mysqli_error($insert);)</script>";
                                                    header("Location: customer_report.php"); 
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>   
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>                
            </div>
        </div>
    </div>
<!-- END PAGE CONTENT INNER -->
</div>
<!-- END PAGE CONTENT BODY -->
<?php include('footer.php') ?>