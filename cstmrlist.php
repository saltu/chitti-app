<?php include('header.php') ;
include("dbfiles/dbcon.php");
$month = date('m');
$day = date('d');
$year = date('Y');
$today = $year . '-' . $month . '-' . $day;
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Add Customer</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE TOOLBAR -->
    </div>
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="home.php">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Customer</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-settings font-red"></i>
                                <span class="caption-subject font-red sbold uppercase">Customer Details</span>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"> </a>
                                <a href="javascript:;" class="reload"> </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-toolbar" style="margin-bottom: 80px !important;">
                                <div class="row">                                    
                                    <div class="alert alert-danger display-hide" id="alert">
                                        <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                    </div>
                                    <div class="alert alert-success display-hide" id="alert2">
                                        <button class="close" data-close="alert"></button> Client Data is successfully Saved!
                                    </div>
                                </div>
                                <div class="row">                                    
                                    <div class="col-md-12">
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <form action="dbfiles/clientadd.php" method="POST" class="form-horizontal">            
                                                       
                                                <div class="row form-body">
                                                <div class="col-md-6">
                                                    <h3><center>Client Details</center></h3>
                                                    <br>
                                                    <div class="form-group">
                                                    <strong>
                                                        <label class="col-md-4 col-md-offset-2 control-label"></label>
                                                        <input type="radio" checked name="payment_type" id="cv" value="daily" class="control-label owner">
                                                            Daily
                                                        <input type="radio" name="payment_type" id="ot" value="monthly" class="control-label owner">
                                                            Monthly
                                                    </strong>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Registration Number</label>
                                                        <div class="col-md-8">
                                                            <input type="number" autocomplete="off" required class="form-control" name="registration_no" id="registration_no" placeholder="Enter Registration Number">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Client Name</label>
                                                        <div class="col-md-8">
                                                            <input type="text" autocomplete="off" required class="form-control" name="name" id="name" placeholder="Enter Client Name">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">House Name</label>
                                                        <div class="col-md-8">
                                                            <input type="text" autocomplete="off" required class="form-control" name="address" id="address" placeholder="Enter House Name">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Place</label>
                                                        <div class="col-md-8">
                                                            <textarea rows="3" autocomplete="off" cols="10" class="form-control place" id="place" name="place" required placeholder="Enter Place"></textarea>
                                                        </div>
                                                    </div>       
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Mobile Number</label>
                                                        <div class="col-md-8">
                                                            <input type="number" autocomplete="off" pattern="[0-9]{10}" required class="form-control" name="mobile" id="mobile" placeholder="Enter Mobile Number">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Phone Number</label>
                                                        <div class="col-md-8">
                                                            <input type="number" autocomplete="off" pattern="[0-9]{7,11}" class="form-control" name="landphone" id="landphone" placeholder="Enter Phone Number">
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Pin</label>
                                                        <div class="col-md-8">
                                                            <input type="number" autocomplete="off" class="form-control" name="pin" id="pin" placeholder="Enter Pin">
                                                        </div>
                                                    </div>                                                     
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Description</label>
                                                        <div class="col-md-8">
                                                            <input type="text" autocomplete="off" class="form-control" name="description" id="description" placeholder="Enter Description">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Amount</label>
                                                        <div class="col-md-8">
                                                            <input type="number" autocomplete="off" required class="form-control" name="debit_amount" id="debit_amount" placeholder="Enter Amount">
                                                        </div>
                                                    </div>

                                                    <div class="form-group dis1">
                                                        <label class="col-md-4 control-label">Total Interest Amount</label>
                                                        <div class="col-md-8">
                                                            <input type="number" autocomplete="off" class="form-control" name="interest" id="interest" placeholder="Interest Amount">
                                                        </div>
                                                    </div>

                                                    <div class="form-group dis2 display-hide">
                                                        <label class="col-md-4 control-label">Interest %</label>
                                                        <div class="col-md-8">
                                                            <input type="number" autocomplete="off" class="form-control" name="interest%" id="interestp" step="any" placeholder="Interest %">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Opening Date</label>
                                                        <div class="col-md-8">
                                                            <input type="date" class="form-control" name="open_date" value="<?php echo $today; ?>" id="open_date" placeholder="Enter Opening Date">
                                                        </div>
                                                    </div>

                                                    <div class="form-group dis1">
                                                        <label class="col-md-4 control-label">Loan Period</label>
                                                        <div class="col-md-8">
                                                            <input type="number" autocomplete="off" class="form-control" name="loan_period" id="loan_period" placeholder="Loan Period">
                                                        </div>
                                                    </div>                                                     
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Closing Date</label>
                                                        <div class="col-md-8">
                                                            <input type="date" required class="form-control" name="close_date" id="close_date" placeholder="Closing Date">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <h3><center>Security Person Details</center></h3>
                                                    <br><br><br>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Name</label>
                                                        <div class="col-md-6">
                                                            <input type="text" autocomplete="off" class="form-control" name="s_name" id="s_name" placeholder="Name">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Address</label>
                                                        <div class="col-md-6">
                                                            <textarea rows="3" cols="10" class="form-control s_address" id="s_address" name="s_address" placeholder="Address"></textarea>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Phone</label>
                                                        <div class="col-md-6">
                                                            <input type="tel" autocomplete="off" pattern="[0-9]{7,15}" class="form-control" name="s_phone" id="s_phone" placeholder="Phone">
                                                        </div>
                                                    </div>                                  

                                                </div>
                                                </div>
                                                <div class="form-actions" style="border-top:0 !important;">
                                                    <div class="row">
                                                        <div class="col-md-offset-4 col-md-9">
                                                            <button type="submit" name="submit" class="btn btn-circle green">Submit</button>
                                                            <a href="cstmrlist.php"><button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- END FORM-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h1>Customers</h1>
                            <table class="table table-striped table-bordered table-hover" id="sample_1">
                                 <thead>
                                    <tr>
                                        <th class ="col-md-1"> Sl.No </th>
                                        <th class ="col-md-1"> Reg No </th>
                                        <th class ="col-md-1"> Name </th> 
                                        <!-- <th> Mobile Number </th> -->
                                        <th class ="col-md-1"> Opening </th>
                                        <th class ="col-md-1"> Closing </th>
                                        <th class ="col-md-1"> Pay Type </th>
                                        <th class ="col-md-1"> Debited Amount </th>
                                        <!-- <th> Paid amount </th>
                                        <th> Interest Amount </th>
                                        <th> Interest % </th> -->
                                        <th class ="col-md-1"> Balance Amount</th>
                                        <th class ="col-md-1"> Edit </th>
                                        <th class ="col-md-1"> Delete </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; 
                                    $sql = mysqli_query($con,"SELECT * FROM clients");
                                    while ($result = mysqli_fetch_array($sql)) {
                                    ?>
                                            <tr id="<?php $result[0]; ?>">
                                                <td class="text-center"> <?php echo $i; ?> </td>
                                                <td class="text-center"> <?php echo $result[1]; ?> </td>
                                                <td class="text-center"> <?php echo $result[2]; ?> </td>
                                                <!-- <td class="text-center"> <?php echo $result[7]; ?> </td> -->
                                                <td class="text-center"> <?php echo $result[12]; ?> </td>
                                                <td class="text-center"> <?php echo $result[13]; ?> </td>
                                                <td class="text-center"> <?php echo $result[9]; ?> </td>
                                                <td class="text-center"> <?php echo $result[10]; ?> </td>
                                                <?php
                                                $totalpaid = 0;
                                                $sql1 = mysqli_query($con,"SELECT * FROM payment_details where customer_regno = '$result[1]'");
                                                while($pay_details = mysqli_fetch_array($sql1)){
                                                    $totalpaid += $pay_details[3];
                                                }
                                                ?>
                                                <!-- <td class="text-center"> <?php echo $totalpaid; ?> </td> -->
                                                <?php 
                                                $tinterest = 0;
                                                $sql2 = mysqli_query($con,"SELECT interest_amount FROM interest_details WHERE registration_no = '".$result[1]."' ");
                                                while($interest = mysqli_fetch_array($sql2)){
                                                    $tinterest += $interest[0];
                                                }
                                                 ?>
                                                <?php if ( $result[9] == "monthly") {
                                                ?>
                                                <!-- <td class="text-center"> <?php echo "NILL"; ?> </td> -->
                                                <!-- <td class="text-center"> <?php echo $result[14]; ?></td> -->
                                                <?php }else{ ?>
                                                <!-- <td class="text-center"> <?php echo $result[14];?> </td> -->
                                                <!-- <td class="text-center"> <?php echo "NILL";?></td> -->
                                                <?php } ?> 
                                                <?php 
                                                $balanceamt = (($result[10] + $tinterest) - $totalpaid);
                                                ?>
                                                <td class="text-center"> <?php echo $balanceamt; ?> </td>
                                                <td class="text-center"><a class="editveh" href="cstmredit.php?id=<?php echo $result[1];?>"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td>
                                                <td class="text-center"><a onclick="javascript:check=confirm( 'Do You Want To Delete?'); if(check==false) return false;" class="deleteveh" href="dbfiles/cstmrdelete.php?id=<?php echo $result[1];?>"> <i class="fa fa-trash" aria-hidden="true"></i> </a></td>
                                            </tr>
                                    <?php
                                        $i++;
                                        }
                                    ?>
                                </tbody>
                            </table>                             
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>                
            </div>
        </div>
    </div>
<!-- END PAGE CONTENT INNER -->
</div>
<!-- END PAGE CONTENT BODY -->
<?php include('footer.php') ?>