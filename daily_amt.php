<?php include('header.php') ?>
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Collection</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="home.php">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Collection</a>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>
                                    </div>
                                    <!-- <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="javascript:;" class="reload"> </a>
                                    </div> -->
                                </div>
                                <div class="portlet-body">
                                    
                                    <div class="row">
                                        <div class="alert alert-danger display-hide" id="alert">
                                            <button class="close" data-close="alert"></button> You have some form errors. Please check below.
                                        </div>
                                        <div class="alert alert-success display-hide" id="alert2">
                                            <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                        </div>
                                    </div>
                                    <!-- BEGIN FORM-->
                                    <form action="amt_daily_update.php" method="GET" class="form-horizontal">
                                    <div class="form-body">
                                        <h3 class="form-section">Collection </h3>
                                        <div class="row">
                                            
                                            <div class="col-md-2"></div>

                                            <div class="col-md-5">
                                                <div class="form-group">
                                                    <label class="control-label col-md-4">Register No</label>
                                                    <div class="col-md-5">
                                                       <input type="text" class="form-control regno" id="regno" name="regno" placeholder="Registration Number">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-5">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Name</label>
                                                        <div class="col-md-6">
                                                           <input type="text" autocomplete="off" class="form-control custname" id="clname" name="custname" placeholder="Customer name">
                                                            <input type="hidden" id="customer_id" name="customer_id" class="form-control form-filter input-sm customer_id" value="0" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <!--/row-->
                                         <br>
                                        <br>
                                        <br>
                                    </div>
                                    <div class="form-actions" style="border-top:0 !important;">
                                        <div class="row">
                                            <div class="col-md-offset-5 col-md-9">
                                                <button type="submit" name="submit" class="btn btn-circle green">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include('footer.php') ?>