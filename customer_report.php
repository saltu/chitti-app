<?php include('header.php') ?>
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Customer Report</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="home.php">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                        <a>Customer</a>
                        <i class="fa fa-circle"></i>
                    </li>

                    <li>
                        <a>Customer Report</a>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->
                <!-- BEGIN PAGE CONTENT INNER -->
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>
                                    </div>
                                    <!-- <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="javascript:;" class="reload"> </a>
                                    </div> -->
                                </div>
                                <div class="portlet-body">
                                    
                                    <div class="row">                                    
                                        <div class="alert alert-danger display-hide" id="alert">
                                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                        </div>
                                        <div class="alert alert-success display-hide" id="alert2">
                                            <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                        </div>
                                    </div>
                                    <!-- BEGIN FORM-->
                                    <form action="customer_reportlist.php" method="GET" class="form-horizontal">
                                        <div class="form-body">
                                            <h3 class="form-section"></h3>
                                            <div class="row">  
                                                <div class="col-md-12">                                              
                                                    <!-- Name -->
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Register Number</label>
                                                            <div class="col-md-6">
                                                                <input type="hidden" required readonly class="form-control custname" id="custname" name="custname" placeholder="Customer name">
                                                                <input type="text" required class="form-control rregno" id="rregno" name="rregno" value="" placeholder="Register No">
                                                            </div>
                                                        </div>
                                                    </div>  
                                                </div>                                                
                                            </div>                                      
                                            <center>
                                            <div class="form-actions">
                                                <button type="submit" id="newsubmitsales" class="btn green newsubmitsales">Submit</button>
                                            </div>
                                            </center>                                           
                                            </div>   
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT INNER -->
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<?php
include("dbfiles/dbcon.php");
$daily = mysqli_query($con,"SELECT * FROM clients WHERE payment_type = 'daily'");
while ($dail = mysqli_fetch_assoc($daily)) {
    $balance = 0;
    $reg = $dail["registration_no"];
    $amount = $dail["total_amount"];
    $payment = mysqli_query($con,"SELECT * FROM payment_details WHERE customer_regno = '$reg' ORDER BY id ASC");
    while ($pay = mysqli_fetch_assoc($payment)) {
        $id = $pay["id"];
        $col_amt = $pay["collected_amount"];
        $balance = $amount - $col_amt;
        $amount = $balance;
        $query2 = "UPDATE payment_details SET balance_amount ='$balance' WHERE id ='$id'";
        mysqli_query($con,$query2);
    }
} 
?>
<?php include('footer.php') ?>