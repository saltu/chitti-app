<?php include('header.php') ;
include("dbfiles/dbcon.php");
$regno = $_GET['regno'];
$typs = mysqli_query($con,"SELECT payment_type FROM clients WHERE registration_no = '$regno'");
$typ = mysqli_fetch_array($typs);
if ($typ[0] == 'monthly') {
    echo "<script>window.location='amt_monthly_update.php?mregno=$regno'</script>";
    die;
}
$c_date = date('Y-m-d');
$dpay = mysqli_query($con,"SELECT * FROM payment_details WHERE customer_regno = '$regno' AND pdate = '$c_date'");
if (mysqli_num_rows($dpay) != 0) {
   echo "<h3><center>Already payed on this day</center></h3>";
   echo "<center><a href='daily_amt.php'><button type='button' class='btn btn-circle blue'>Go Back</button></a></center> ";
   die();
}
?>
<!-- END HEADER -->
<!-- BEGIN CONTAINER -->
<div class="page-container">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <!-- BEGIN PAGE HEAD-->
        <div class="page-head">
            <div class="container">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Daily Client</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
        </div>
        <!-- END PAGE HEAD-->
        <!-- BEGIN PAGE CONTENT BODY -->
        <div class="page-content">
            <div class="container">
                <!-- BEGIN PAGE BREADCRUMBS -->
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <a href="home.php">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="#">Customer Daily Update</a>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMBS -->            
                <div class="page-content-inner">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box green">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-gift"></i>
                                    </div>
                                    <!-- <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        <a href="javascript:;" class="reload"> </a>
                                    </div> -->
                                </div>
                                <div class="portlet-body">
                                    
                                    <div class="row">                                    
                                        <div class="alert alert-danger display-hide" id="alert">
                                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                        </div>
                                        <div class="alert alert-success display-hide" id="alert2">
                                            <button class="close" data-close="alert"></button> Your Data is successful Saved!
                                        </div>
                                    </div>
                                    <!-- BEGIN FORM-->
                                    <form action="#" method="POST" class="form-horizontal" id="dailyc">
                                        <div class="form-body">
                                            <h3 class="form-section">Info</h3>
                                            <div class="row">
                                            <div class="col-md-8">
                                                <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Date</label>
                                                        <div class="col-md-8">
                                                            <?php 
                                                                $month = date('m');
                                                                $day = date('d');
                                                                $year = date('Y');

                                                                $today = $year . '-' . $month . '-' . $day;
                                                            ?>
                                                            <input type="date" value="<?php echo $today; ?>" class="form-control" id="cudate" name="cudate" required placeholder="Date">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                
                                                <!-- Bill number automatic -->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Registration Number</label>
                                                        <div class="col-md-8">
                                                            <input type="text" required class="form-control regno" id="regno" name="regno" value="<?php echo $regno; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <!-- Name -->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Name</label>
                                                        <div class="col-md-8">
                                                            <input type="text" required readonly class="form-control custname" id="custname" name="custname" placeholder="Customer name">
                                                            <input type="hidden" id="customer_id" name="customer_id" class="form-control form-filter input-sm customer_id" value="0" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <!-- phone number  -->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Opening Date</label>
                                                        <div class="col-md-8">
                                                            <input type="date" readonly class="form-control open_date" id="open_date" name="open_date">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <!-- phone number  -->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">Closing Date</label>
                                                        <div class="col-md-8">
                                                            <input type="date" readonly value="0" class="form-control close_date" id="close_date" name="close_date">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="amt_table" class="col-md-4">
                                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th> Sl.No </th>
                                                <th> Date </th>
                                                <th> Amount </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; 
                                            $sql = mysqli_query($con,"SELECT * FROM payment_details WHERE customer_regno = '$regno' ORDER BY pdate DESC");
                                            while ($results = mysqli_fetch_assoc($sql)) {
                                                                            ?>
                                                <tr id="<?php $results['id']; ?>">
                                                    <td class="text-center"> <?php echo $i; ?> </td>
                                                    <td class="text-center"> <?php echo $results['pdate']; ?>
                                                    </td>
                                                    <td class="text-center"> <?php echo $results['collected_amount']; ?> </td>
                                            <?php
                                                $i++;
                                            }
                                            ?>
                                        </tbody>
                                        </table>
                                            
                                        </div>
                                        </div>
                                            <!--/row-->
                                            <div class="row">
                                            <h3 class="form-section col-md-7">Details</h3>
                                            <h3 class="form-section col-md-4">Days Remaining : <span class="r_days" id="r_days"></span></h3>
                                            </div>
                                            <!--/row-->
                                            <div class="row" >
                                                <div class="col-md-12">
                                                    <div class="portlet-body">
                                                        <div class="table-responsive" style="height:150px !important;" >
                                                            <table class="table table-striped table-bordered table-hover">
                                                                <thead>
                                                                    <tr class="d-flex" >
                                                                        <th class ="text-center col-md-2"> Amount Debited </th>
                                                                        <th class ="text-center col-md-2"> Interest Amount </th>
                                                                        <th class ="text-center col-md-2"> Total Amount </th>
                                                                        <th class ="text-center col-md-2"> Amount Paid </th>
                                                                        <th class="text-center"> Today's Payment </th>
                                                                        <th class="text-center"> Amount to be paid </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                   <tr id="sales-table">
                                                                        <td><input required type="number" id="damnt" name="damnt" readonly class="form-control form-filter input-sm damnt" value="0"> 
                                                                        </td>
                                                                        <td><input required type="number" id="intr" name="intr" readonly class="form-control form-filter input-sm intr" value="0"></td>

                                                                        <td><input required type="number" id="tot_amt" name="tot_amt" readonly class="form-control form-filter input-sm vehiclenum">
                                                                        </td>

                                                                        <td><input required type="number" id="amt_paid" name="Amount Paid" readonly class="form-control form-filter input-sm itemname" value="0">
                                                                        </td>

                                                                        <td> <input required type="number" class="form-control form-filter input-sm" id="tdy_pymt" name="tdy_pymt">
                                                                        <input required type="hidden" id="interest" name="interest" readonly class="form-control form-filter input-sm interest" value="0">
                                                                        </td>

                                                                        <td> <input required type="hidden" class="form-control form-filter input-sm" id="bal_amt" readonly name="bal_amt">
                                                                        <input required type="number" class="form-control form-filter input-sm dbal_amt" id="dbal_amt" readonly name="dbal_amt">
                                                                        </td>
                                                                    <!-- <tr>
                                                                        <td><input type="radio" checked value="scash" id="scash" name="smode" class="scash" /> CASH
                                                                        </td>
                                                                        <td><input type="radio" value="scheque" id="scheque" name="smode" class="scheque" /> CHEQUE
                                                                        </td> 
                                                                        <td> <input placeholder="Cheque Number" type="text" class="sref display-hide form-control form-filter input-sm" value="0" id="sref" name="sref"> </td>
                                                                    </tr> -->
                                                                </tbody>
                                                            </table>                                                            
                                                        </div>                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/row-->
                                        </div>
                                        <center>
                                        <div class="form-actions">
                                            <button type="submit" name="submit" id="submitsalesbil" class="btn green ">Submit</button>
                                            <button type="button" id="cancelsalesbil" class="btn default ">Cancel</button>

                                            <div class="row">                                    
                                                <div class="alert alert-danger display-hide" id="alertsalespay">
                                                    <button class="close" data-close="alert"></button> The Amount you entered is higher. 
                                                </div>
                                            </div>
                                        </div>
                                        </center>
                                    </form>
                                    <?php
                                    // $insert1 = 0;
                                    if(isset($_POST["submit"])){
                                        $tdpay = "INSERT INTO payment_details(pdate,customer_regno,collected_amount,balance_amount,mcheck_status) VALUES('".$_POST['cudate']."','".$_POST['regno']."','".$_POST['tdy_pymt']."','".$_POST['dbal_amt']."','1')";
                                        $insert = mysqli_query($con,$tdpay) or die(mysqli_error($con));
                                        // if ($_POST['interest'] != 0) { 
                                        // $query1 = "INSERT INTO interest_details(registration_no,interest_amount,cdate) VALUES('".$_POST['regno']."','".$_POST['interest']."','".$_POST['cudate']."')";
                                        // $insert1 = mysqli_query($con,$query1);
                                        // }
                                        // if (!$insert || !$insert1) {
                                        // printf("Error: %s\n", mysqli_error($con));
                                        // exit();
                                        // }
                                        if($insert){
                                            echo "<script>alert('Amount added')</script>";
                                            echo "<script>window.location='daily_amt.php'</script>";
                                        }
                                        else{
                                            echo "<script>alert('You have some form errors. Please check below')</script>";
                                            echo "<script>window.location='amt_daily_update.php'</script>";
                                            // header("Location: amt_daily_update.php"); 
                                        }

                                    }
                                    ?>
                                    <!-- END FORM-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT BODY -->
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
<?php include('footer.php') ?>
<script type="text/javascript">
    $(document).ready(function(){
    var value = $("#regno").val();
    $.ajax({
            type:'get',
            url:'dbfiles/paymentdtls.php',
            data:{value:value},
            success:function(data)
            {
                var json = $.parseJSON(data);
                var reg = json['reg'];
                // alert(json['id']);
                $('#custname').val(json['name']);
                $('#customer_id').val(json['id']);
                $('#amt_paid').val(json['amt_paid']);
                $('#open_date').val(json['o_date']);
                $('#close_date').val(json['c_date']);
                $('#damnt').val(json['damnt']);
                $('#intr').val(json['intr']);
                $('#tot_amt').val(json['total_amount']);
                $('#bal_amt').val(json['balance']);
                $('#dbal_amt').val(json['balance']);
                $('#r_days').html(json['r_days']);
                if (json['block'] == '0' ) {
                    $('#dailyc').html("Closing Date Exceeded <a style='margin-left: 30%;' href='daily_amt.php'><button type='button' class='btn btn-circle blue'>Back</button></a>");
                }
            }
        });
    $("#tdy_pymt").focus();
    });
</script>