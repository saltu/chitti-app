<?php include('header.php') ?>
    <!-- END HEADER -->        
    <!-- BEGIN CONTAINER -->
    <div class="page-container">

    <div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Dashboard
                </h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
    </div>
    <!-- END PAGE HEAD-->
    <!-- BEGIN PAGE CONTENT BODY -->
    <div class="page-content">
        <div class="container">
            <!-- BEGIN PAGE BREADCRUMBS -->
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Dashboard</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMBS -->
            <!-- BEGIN PAGE CONTENT INNER -->
            <?php include("dbfiles/dbcon.php");
            $date = date('Y-m-d');
            $todaydebit = 0;
            $totdebit = 0;
            $todaycoltn = 0;
            $totcoltn = 0;
            $totint = 0;
            $tint = 0;
            $sql = mysqli_query($con,"SELECT * FROM clients WHERE open_date = '$date' ");
            if (!$sql) {
            printf("Error: %s\n", mysqli_error($con));
            exit();
            }
            while($tddebit = mysqli_fetch_array($sql)){
                $todaydebit += $tddebit[10];
            }
            $sql1 = mysqli_query($con,"SELECT * FROM clients");
            while($client = mysqli_fetch_array($sql1)){
                $totdebit += $client[10];
            }
            $sql2 = mysqli_query($con,"SELECT * FROM payment_details WHERE pdate = '$date'");
            while($todaycltn = mysqli_fetch_array($sql2)){
                $todaycoltn += $todaycltn[3];
            }
            $sql3 = mysqli_query($con,"SELECT * FROM payment_details");
            while($totcltn = mysqli_fetch_array($sql3)){
                $totcoltn += $totcltn[3];
            }
            $sql4 = mysqli_query($con,"SELECT * FROM interest_details");
            while($int = mysqli_fetch_array($sql4)){
                $totint += $int[2];
            }
            $sql5 = mysqli_query($con,"SELECT * FROM interest_details WHERE cdate = '$date'");
            while($todayint = mysqli_fetch_array($sql5)){
                $tint += $todayint[2];
            }

            $dateto = date("Y-m-d",strtotime(date("Y-m-d",strtotime($date)). " +1 month"));
            $sql6 = mysqli_query($con,"SELECT * FROM clients WHERE payment_type NOT IN ('daily') AND close_date BETWEEN '$date' AND '$dateto'");
                if (!$sql6) {
                printf("Error: %s\n", mysqli_error($con));
            exit();
                }
                $count0 = mysqli_num_rows($sql6);

            // $wdateto = date("Y-m-d",strtotime(date("Y-m-d",strtotime($date)). " +1 week"));
            //         $sql7 = mysqli_query($con,"SELECT * FROM clients WHERE payment_type NOT IN ('monthly') AND close_date BETWEEN '$date' AND '$wdateto'");
            //         if (!$sql7) {
            //         printf("Error: %s\n", mysqli_error($con));
            //         exit();
            //         }
            //         $count1 = mysqli_num_rows($sql7);
            $count = $count0;
            ?>
            <div class="page-content-inner">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-10">
                            <div class="dashboard-stat blue">
                                <div class="visual">
                                    <i class="fa fa-briefcase fa-icon-medium"></i>
                                </div>
                                <div class="details">
                                   
                                    <div class="number"> <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $todaydebit; ?> </div>
                                    <div class="desc">Today's Debited</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat green">
                                <div class="visual">
                                    <i class="fa fa-group fa-icon-medium"></i>
                                </div>
                                <div class="details">
                                    
                                    <div class="number"> <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $todaycoltn; ?></div>
                                    <div class="desc">Today's Collection</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat purple" >
                                <div class="visual">
                                    <i class="fa fa-balance-scale fa-icon-medium"></i>
                                </div>
                                <div class="details">
                                   
                                    <div class="number"> <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $tint; ?></div>
                                    <div class="desc"> Today's Interest </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat ngreen" >
                                <div class="visual opacity">
                                    <i class="fa fa-credit-card fa-icon-medium"></i>
                                </div>
                                <div class="details">
                                    
                                    <div class="number"> <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $totdebit; ?></div>
                                    <div class="desc">Total Debited</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat dpink" >
                                <div class="visual opacity">
                                    <i class="fa fa-pie-chart fa-icon-medium"></i>
                                </div>
                                <div class="details">
                                    
                                    <div class="number"> <i class="fa fa-inr" aria-hidden="true"></i> <?php echo $totint; ?></div>
                                    <div class="desc">Total Interest Gained </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="dashboard-stat yellow" >
                                <div class="visual">
                                    <i class="fa fa-anchor fa-icon-medium"></i>
                                </div>
                                <div class="details">
                                    
                                    <div class="number"> <i class="fa fa-inr" aria-hidden="true"></i><?php echo $totcoltn; ?></div>
                                    <div class="desc">Total Collected</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12" id="myBtn">
                            <div class="dashboard-stat lgreen" >
                                <div class="visual opacity">
                                    <i class="fa fa fa-flag fa-icon-medium"></i>
                                </div>
                                <div class="details">
                                    
                                    <div class="number"><?php echo $count; ?> <i class="fa fa-bell fa-icon-small" aria-hidden="true"></i></div>
                                    <div class="desc">Notifications</div>
                                </div>
                            </div>
                        </div>
                    </div>
            <div id="myModal" class="modal">

            <!-- Modal content -->
            <div class="modal-content">
                <span class="close">&times;</span>
                <span class="caption-subject font-red sbold uppercase">Notifications</span><br>
                <span class="caption ml-5 btn btn-success im">Closing Chitti Notification(Monthly) </span>
                <!-- <span class="caption ml-5 btn btn-warning 1">Closing Chitti Notification(Daily)</span> -->
                <br><br><br>
                <div class="portlet-body" id="samplef">
                <!-- <h2>Closing Chitti</h2>  -->                           
                <table class="table table-striped table-bordered table-hover" >
                    <thead>
                        <tr>
                            <th class="text-center"> Sl.No </th>
                            <th class="text-center"> Name </th>
                            <th class="text-center"> Register Number </th>
                            <th class="text-center"> Open Date </th>
                            <th class="text-center"> Closing Date </th>
                            <th class="text-center"> Debit Amount </th>
                            <!-- <th class="text-center"> Edit </th> -->
                        </tr>
                    </thead>
                    <tbody>
                    <?php $i = 1;
                   
                    while($clschitty = mysqli_fetch_array($sql6)){
                    ?>
                   
                        <tr id="{{ $notifyy->id }}">
                            <td class="text-center"> <?php echo $i;?> </td>
                            <td class="text-center"> <?php echo "$clschitty[2]";?></td>
                            <td class="text-center"> <?php echo "$clschitty[1]";?></td>
                            <td class="text-center"> <?php echo "$clschitty[11]";?></td>
                            <td class="text-center"> <?php echo "$clschitty[12]";?></td>
                            <td class="text-center"> <?php echo "$clschitty[10]";?></td>
                            <!-- <td class="text-center"><a class="editveh" href="{{ route('vehicle.edit', $notifyy->id ) }}"> <i class="fa fa-pencil-square-o" aria-hidden="true"></i> </a></td> -->
                        </tr>

                    <?php
                    $i++;
                    }
                    ?>

                    </tbody>
                </table>
                </div>
                
        </div>
        </div> 
            <!-- END CONTENT -->
            </div>
            <!-- END PAGE CONTENT INNER -->
        </div>
    </div>
</div>
    <!-- END PAGE CONTENT BODY -->
    <!-- END CONTENT BODY -->
</div>
    <!-- END CONTAINER -->
<?php
$mdate = date('m');
$cur_month = date('m', strtotime(date('Y-m')));
$du_date = mysqli_query($con,"SELECT due_date,registration_no FROM minterest_details WHERE Month(due_date) = '$cur_month'");
while ($months = mysqli_fetch_array($du_date)) {
    $mth = $months[0];
    $rno = $months[1];
    $thedate = explode("-", $mth);
    $month = $thedate[1];
    $year = $thedate[0];
// if ( $month <= $mdate ) {
    $sql7 = mysqli_query($con,"SELECT * FROM minterest_details WHERE registration_no = '$rno'");
    if (!$sql7) {
        printf("Error: %s\n", mysqli_error($con));
        exit();
    }
    while($clientdtls = mysqli_fetch_array($sql7)){
        $balanc_amt = 0;
        $regno = $clientdtls[3];
        $cdate = $clientdtls[2];
        $mcdate = date("Y-m-d",strtotime(date("Y-m-d",strtotime($cdate)). " +1 month"));
        $intamt = $clientdtls[5];
        $bintamt = $clientdtls[6];
        $ddate = $clientdtls[7];
        $mddate = date("Y-m-d",strtotime(date("Y-m-d",strtotime($ddate)). " +1 month"));
        $intp = mysqli_query($con,"SELECT interest,debit_loan FROM clients WHERE registration_no = '$regno'");
        $intrp = mysqli_fetch_array($intp);
        // $sql8 = mysqli_query($con,"SELECT debit_loan FROM clients WHERE customer_regno = '$regno'");
        //         while($baln_amount = mysqli_fetch_array($sql8)){
        //             $bln_amt = 0;
        //             $bln_amt = $baln_amount[0];
        //         }
        if ($bintamt == 0) {
            $balanc_amt = $intrp[1];
        }else{
            $balanc_amt = $intrp[1] + $bintamt;
        }
        $query0 = "UPDATE clients SET debit_loan ='$balanc_amt' WHERE registration_no ='$regno'";
        mysqli_query($con,$query0);
        $interest_amount = ($balanc_amt * $intrp[0])/100;
        $query1 = "UPDATE minterest_details SET cdate ='$mcdate', interest_amount ='$interest_amount', balinterest_amount ='$interest_amount', due_date ='$mddate' WHERE registration_no ='$regno'";
        mysqli_query($con,$query1);
    }
// }
}

$daily = mysqli_query($con,"SELECT * FROM clients WHERE payment_type = 'daily'");
while ($dail = mysqli_fetch_assoc($daily)) {
    $balance = 0;
    $reg = $dail["registration_no"];
    $amount = $dail["total_amount"];
    $payment = mysqli_query($con,"SELECT * FROM payment_details WHERE customer_regno = '$reg' ORDER BY id ASC");
    while ($pay = mysqli_fetch_assoc($payment)) {
        $id = $pay["id"];
        $col_amt = $pay["collected_amount"];
        $balance = $amount - $col_amt;
        $amount = $balance;
        $query2 = "UPDATE payment_details SET balance_amount ='$balance' WHERE id ='$id'";
        mysqli_query($con,$query2);
    }
}
?>
<?php include('footer.php') ?>