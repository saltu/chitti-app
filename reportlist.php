<?php include('header.php') ;
include("dbfiles/dbcon.php");
$rdate = $_POST['dailydate'];
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Daily Report</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE TOOLBAR -->
    </div>
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="home.php">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Daily Report</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-settings font-red"></i>
                                <span class="caption-subject font-red sbold uppercase"><?php echo $rdate ?> Day Report</span>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"> </a>
                                <a href="javascript:;" class="reload"> </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover" id="sample_1">
                                <thead>
                                    <tr>
                                        <th class="text-center"> Sl.No </th>
                                        <th class="text-center"> Register Number </th>
                                        <th class="text-center"> Customer Name </th>
                                        <th class="text-center"> Payment Type </th> 
                                        <th class="text-center"> Details </th> 
                                        <th class="text-center"> Amount </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="success">
                                        <td colspan="6" class="text-center">DEBIT</td>
                                    </tr>
                                    <?php $i = 1;
                                    $j = 1;
                                    $k = 1;
                                    $tot_debit = 0;
                                    $tot_collected = 0; 
                                    $tot_intr =0;
                                        $sql = mysqli_query($con,"SELECT * FROM clients WHERE open_date = '$rdate'");
                                        $count = mysqli_num_rows($sql);
                                    if ($count != 0) {
                                    while ($result = mysqli_fetch_array($sql)) {
                                    ?>
                                        <tr>
                                            <td class="text-center"> <?php echo $i; ?> </td>
                                            <td class="text-center"> <?php echo $result[1]; ?> </td>
                                            <td class="text-center"> <?php echo $result[2]; ?> </td>
                                            <td class="text-center"> <?php echo $result[9]; ?> </td>
                                            <td class="text-center">New</td>
                                            <td class="text-center"> <?php echo $result[10]; ?> </td>
                                            <?php
                                            $tot_debit +=$result[10];
                                            ?>
                                        </tr>
                                    <?php
                                        $i++;
                                        }
                                    }else{ ?>
                                        <tr class="warning">
                                            <td class="text-center"><?php echo $i; ?></td>
                                            <td colspan="5" class="text-center">Nobody had debited no this date</td>
                                        </tr>
                                    <?php } ?>
                                    <tr class="success">
                                        <td colspan="6" class="text-center">COLLECTION</td>
                                    </tr>
                                    <?php
                                    $sql1 = mysqli_query($con,"SELECT * FROM payment_details WHERE pdate = '$rdate' AND collected_amount != 0");
                                    $count1 = mysqli_num_rows($sql1);
                                    if ($count1 != 0) {
                                    while ($result1 = mysqli_fetch_array($sql1)) {
                                    $sql2 = mysqli_query($con,"SELECT * FROM clients WHERE registration_no = '$result1[2]'");
                                    $cdtls = mysqli_fetch_array($sql2);
                                    ?>
                                        <tr>
                                            <td class="text-center"> <?php echo $j; ?> </td>
                                            <td class="text-center"> <?php echo $result1[2]; ?> </td>
                                            <td class="text-center"> <?php echo $cdtls[2]; ?> </td>
                                            <td class="text-center"> <?php echo $cdtls[9]; ?> </td>
                                            <td class="text-center">Collection</td>
                                            <td class="text-center"> <?php echo $result1[3]; ?></td>
                                            <?php $tot_collected +=$result1[3]; ?>
                                        </tr>
                                    <?php
                                        $j++;
                                        }
                                    }else{ ?>
                                        <tr class="warning">
                                            <td class="text-center"><?php echo $j; ?></td>
                                            <td colspan="5" class="text-center">Nobody had Paid no this date</td>
                                        </tr>
                                    <?php } ?>
                                    <tr class="success">
                                        <td colspan="6" class="text-center">INTEREST</td>
                                    </tr>
                                    <?php
                                    $sql3 = mysqli_query($con,"SELECT * FROM interest_details WHERE cdate = '$rdate'");
                                    $count2 = mysqli_num_rows($sql3);
                                    if ($count2 != 0) {
                                        while($intr = mysqli_fetch_array($sql3)){
                                        $sql4 = mysqli_query($con,"SELECT * FROM clients WHERE registration_no = '$intr[1]'");
                                        $cdts = mysqli_fetch_array($sql4);
                                    ?>
                                                <tr>
                                                    <td class="text-center"> <?php echo $k; ?> </td>
                                                    <td class="text-center"> <?php echo $intr[1]; ?> </td>
                                                    <td class="text-center"> <?php echo $cdts[2]; ?> </td>
                                                    <td class="text-center"> <?php echo $cdts[9]; ?> </td>
                                                    <td class="text-center"> Interest </td>
                                                    <td class="text-center"> <?php echo $intr[2]; ?></td>
                                                    <?php $tot_intr +=$intr[2]; ?>
                                                </tr>
                                        <?php
                                            $k++;
                                        }
                                    }else{ ?>
                                        <tr class="warning">
                                            <td class="text-center"><?php echo $k; ?></td>
                                            <td colspan="5" class="text-center">No Interest gained on this day</td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <td colspan="3" class="text-right"><b>Total Debited</b></td>
                                        <td colspan="3" class="text-left"><?php echo $tot_debit; ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="text-right"><b>Toatal Collected(Inc Interest)</b></td>
                                        <td colspan="3" class="text-left"><?php echo $tot_collected; ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>                
            </div>
        </div>
    </div>
<!-- END PAGE CONTENT INNER -->
</div>
<!-- END PAGE CONTENT BODY -->
<?php include('footer.php') ?>