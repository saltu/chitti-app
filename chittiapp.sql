-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 10, 2019 at 12:41 PM
-- Server version: 5.7.26
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chittiapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `Account` float(50,2) NOT NULL,
  `debit` float(50,2) NOT NULL,
  `credit` float(50,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `username`, `password`) VALUES
(1, 'admin', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(11) NOT NULL,
  `registration_no` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `place` varchar(100) NOT NULL,
  `address` varchar(255) NOT NULL,
  `pin` int(20) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `landphone` varchar(15) DEFAULT NULL,
  `payment_type` char(20) NOT NULL,
  `debit_amount` int(50) NOT NULL,
  `debit_loan` float(10,1) DEFAULT NULL,
  `open_date` date NOT NULL,
  `close_date` date NOT NULL,
  `interest` float(10,1) DEFAULT NULL,
  `total_amount` float(50,1) NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `extended_duration` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `registration_no`, `name`, `description`, `place`, `address`, `pin`, `mobile`, `landphone`, `payment_type`, `debit_amount`, `debit_loan`, `open_date`, `close_date`, `interest`, `total_amount`, `duration`, `extended_duration`) VALUES
(1, '123', 'Tester1', '', 'qwwe', 'qwee', 690516, '1234567890', '1234567890', 'daily', 10000, NULL, '2019-10-19', '2019-11-19', 1500.0, 11500.0, NULL, NULL),
(3, '344', 'qwqwq', 'wfs', 'sdf', 'sfd', 1212121, '12121', '12112121', 'daily', 50000, NULL, '2019-10-21', '2019-10-30', 2000.0, 52000.0, NULL, NULL),
(4, '1212', 'Prithviraj.K.R', 'fghdjfgj', 'nangiarkulangara', 'creative suite', 690506, '2147483647', '2147483647', 'daily', 2000, 2000.0, '2019-10-21', '2019-12-31', 200.0, 2200.0, NULL, NULL),
(6, '12345', 'sgazefa', 'kjhbukyv', 'nangiyarkulangara', 'keeerthy', 690514, '7012525742', '', 'monthly', 2000, 2000.0, '2019-12-09', '2020-01-08', 2.5, 2000.0, 0, 0),
(11, '1111', 'noufal', 'kjhbukyv', 'Nagiarkulangara', 'creative suite', 690514, '7012525743', '2475563', 'daily', 2000, 2000.0, '2019-10-31', '2019-11-30', 200.0, 2200.0, 20, 10),
(17, '12351', 'Akash Sanjeev', 'fghdjfgj', 'svzcvsavs', 'creative suite', 690513, '7012524157', '2476355', 'monthly', 2000, 2597.5, '2019-10-06', '2020-03-06', 2.5, 2000.0, 0, 0),
(21, '1112', 'nithin', 'rfhdfctgh', 'karthikapally', 'saltu', 690507, '701252751', '', 'monthly', 3000, 3000.0, '2019-12-06', '2020-04-09', 2.5, 3000.0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `interest_details`
--

CREATE TABLE `interest_details` (
  `id` int(11) NOT NULL,
  `registration_no` varchar(100) DEFAULT NULL,
  `interest_amount` float(50,1) DEFAULT NULL,
  `cdate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `interest_details`
--

INSERT INTO `interest_details` (`id`, `registration_no`, `interest_amount`, `cdate`) VALUES
(20, '12351', 50.0, '2019-10-06'),
(21, '12351', 60.0, '2019-11-10'),
(22, '12351', 63.0, '2019-12-03'),
(23, '1112', 28.0, '2019-12-14');

-- --------------------------------------------------------

--
-- Table structure for table `minterest_details`
--

CREATE TABLE `minterest_details` (
  `id` int(11) NOT NULL,
  `idate` date DEFAULT NULL,
  `cdate` date DEFAULT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `interest` float(10,1) DEFAULT NULL,
  `interest_amount` float(50,1) DEFAULT NULL,
  `balinterest_amount` float(50,1) DEFAULT NULL,
  `due_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `minterest_details`
--

INSERT INTO `minterest_details` (`id`, `idate`, `cdate`, `registration_no`, `interest`, `interest_amount`, `balinterest_amount`, `due_date`) VALUES
(5, '2019-10-06', '2019-10-06', '12351', 2.5, 50.0, 50.0, '2019-11-06'),
(9, '2019-12-06', '2019-12-06', '1112', 2.5, 75.0, 47.0, '2020-01-06'),
(11, '2019-12-09', '2019-12-09', '12345', 2.5, 50.0, 50.0, '2020-01-09');

-- --------------------------------------------------------

--
-- Table structure for table `payment_details`
--

CREATE TABLE `payment_details` (
  `id` int(20) NOT NULL,
  `pdate` date DEFAULT NULL,
  `customer_regno` int(20) DEFAULT NULL,
  `collected_amount` float(50,2) DEFAULT NULL,
  `interest_amount` float(50,2) DEFAULT NULL,
  `loan_amount` float(20,2) DEFAULT NULL,
  `balance_amount` float(20,2) DEFAULT NULL,
  `mcheck_status` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
--
-- Dumping data for table `payment_details`
--

INSERT INTO `payment_details` (`id`, `pdate`, `customer_regno`, `collected_amount`, `interest_amount`, `loan_amount`, `balance_amount`, `mcheck_status`) VALUES
(1, '2019-10-21', 123, 500.0, NULL, NULL, 11000.0, 1),
(8, '2019-10-22', 1212, 200.0, NULL, NULL, 2000.0, 1),
(9, '2019-10-22', 1212, 500.0, NULL, NULL, 1500.0, 1),
(27, '2019-10-06', 12351, NULL, NULL, NULL, 3000.0, 1),
(30, '2019-10-06', 12351, 50.0, 50.0, 0.0, 0.0, 1),
(34, '2019-11-09', 1212, 100.0, NULL, NULL, 1400.0, 1),
(35, '2019-11-10', 12351, 60.0, 60.0, 0.0, 0.0, 1),
(37, '2019-12-03', 12351, 113.0, 63.0, 50.0, 0.0, 0),
(41, '2019-12-06', 1112, NULL, NULL, NULL, 3000.0, 1),
(43, '2019-12-09', 12345, NULL, NULL, NULL, 2000.0, 1),
(44, '2019-12-14', 1112, 28.0, 28.0, 0.0, 0.0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `security_person`
--

CREATE TABLE `security_person` (
  `id` int(11) NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `name` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `security_person`
--

INSERT INTO `security_person` (`id`, `client_id`, `name`, `address`, `phone`) VALUES
(1, '1111', 'Akash Sanjeev', 'Akku Bhavanam Haripad', '9446858804'),
(7, '12351', '', '', ''),
(11, '1112', '', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interest_details`
--
ALTER TABLE `interest_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `minterest_details`
--
ALTER TABLE `minterest_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_details`
--
ALTER TABLE `payment_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `security_person`
--
ALTER TABLE `security_person`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `interest_details`
--
ALTER TABLE `interest_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `minterest_details`
--
ALTER TABLE `minterest_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `payment_details`
--
ALTER TABLE `payment_details`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `security_person`
--
ALTER TABLE `security_person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
