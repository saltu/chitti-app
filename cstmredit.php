<?php include('header.php');
include("dbfiles/dbcon.php");

$id =$_GET['id'];
$sql = mysqli_query($con,"SELECT * FROM clients WHERE registration_no = '$id'");
while ($cdtls = mysqli_fetch_array($sql)) {
    $type = $cdtls[9];
    if ($type == 'daily') {
       $sql1 = mysqli_query($con,"SELECT * FROM interest_details WHERE registration_no = '$id'");
       while ($idtls = mysqli_fetch_array($sql1)) {
            $iid = $idtls[0];
            $interest_amount = $idtls[2];
        }
    }
    $name = $cdtls[2];
    $regno = $cdtls[1];
    $amount = $cdtls[10];
    $house = $cdtls[5];
    $place = $cdtls[4];
    $mobile = $cdtls[7];
    $lphone = $cdtls[8];
    $pin = $cdtls[6];
    $desc = $cdtls[3];
    $interest = $cdtls[14];
    $opndate = $cdtls[12];
    $clodate = $cdtls[13];
    $total_amount = $cdtls[15];
    $loan_period = $cdtls[16];
    $ext_duration = $cdtls[17];
}
$sql1 = mysqli_query($con,"SELECT * FROM security_person WHERE client_id = '$id'");
$s_dtls = mysqli_fetch_assoc($sql1);
$s_name = $s_dtls["name"];
$s_address = $s_dtls["address"];
$s_phone = $s_dtls["phone"];
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Add Customer</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE TOOLBAR -->
    </div>
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="home.php">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Customer</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-settings font-red"></i>
                                <span class="caption-subject font-red sbold uppercase">Customer Details</span>
                            </div>
                            <div class="tools">
                                <a href="renew_customer.php?id=<?php echo $regno;?>"><i class="fa fa-cog fa-2x" aria-hidden="true"></i>&nbsp Renew Client</a>
                                <a href="javascript:;" class="collapse"> </a>
                                <a href="javascript:;" class="reload"> </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-toolbar" style="margin-bottom: 80px !important;">
                                <div class="row">                                    
                                    <div class="alert alert-danger display-hide" id="alert">
                                        <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                    </div>
                                    <div class="alert alert-success display-hide" id="alert2">
                                        <button class="close" data-close="alert"></button> Client Data is successfully Saved!
                                    </div>
                                </div>
                                <div class="row">                                    
                                    <div class="col-md-12">
                                        <div class="portlet-body form">
                                            <!-- BEGIN FORM-->
                                            <form action="#" method="POST" class="form-horizontal">            
                                            <div class="row form-body">
                                                <div class="col-md-6">
                                                    <h3><center>Client Details</center></h3>
                                                    <br>
                                                    <div class="form-group">
                                                    <strong>
                                                        <label class="col-md-4 col-md-offset-2 control-label"></label>
                                                        <?php
                                                        if ($type == 'daily') { 
                                                        ?>
                                                        <input type="radio" checked name="payment_type" id="cv" value="daily" class="control-label owner">
                                                            Daily
                                                        <?php }
                                                        else { 
                                                        ?>
                                                        <input type="radio" checked name="payment_type" id="ot" value="monthly" class="control-label owner">
                                                            Monthly
                                                        <?php } ?>
                                                    </strong>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Client Name</label>
                                                        <div class="col-md-8">
                                                            <input type="text" required class="form-control" name="name" id="name" value="<?php echo $name; ?>" placeholder="Enter Client Name">
                                                        </div>
                                                    </div>

                                                    

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Registration Number</label>
                                                        <div class="col-md-8">
                                                            <input type="number" required readonly class="form-control" name="registration_no" id="registration_no" value="<?php echo $regno; ?>">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">House Name</label>
                                                        <div class="col-md-8">
                                                            <input type="text" required class="form-control" name="address" id="address" value="<?php echo $house; ?>" placeholder="Enter House Name">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Place</label>
                                                        <div class="col-md-8">
                                                            <input type="text" required class="form-control" name="place" id="place" value="<?php echo $place; ?>" placeholder="Enter Place">
                                                        </div>
                                                    </div>       
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Mobile Number</label>
                                                        <div class="col-md-8">
                                                            <input type="number" required class="form-control" name="mobile" id="mobile" value="<?php echo $mobile; ?>" placeholder="Enter Mobile Number">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Phone Number</label>
                                                        <div class="col-md-8">
                                                            <input type="number" class="form-control" name="landphone" id="landphone" value="<?php echo $lphone; ?>" placeholder="Enter Phone Number">
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Pin</label>
                                                        <div class="col-md-8">
                                                            <input type="number" class="form-control" name="pin" id="pin" value="<?php echo $pin; ?>" placeholder="Enter Pin">
                                                        </div>
                                                    </div>                                                     
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Description</label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" name="description" id="description" value="<?php echo $desc; ?>" placeholder="Enter Description">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Amount</label>
                                                        <div class="col-md-8">
                                                            <input type="number"  class="form-control" name="debit_amount" id="debit_amount" value="<?php echo $amount; ?>" step="any">
                                                        </div>
                                                    </div>

                                                    <?php
                                                        if ($type == 'daily') { 
                                                    ?>
                                                    <div class="form-group dis1">
                                                        <label class="col-md-4 control-label">Interest Amount</label>
                                                        <div class="col-md-8">
                                                            <input type="number" class="form-control" name="interest" id="interest" value="<?php echo $interest; ?>" placeholder="Interest Amount">
                                                        </div>
                                                    </div>
                                                    <?php }
                                                        else { 
                                                    ?>
                                                    <div class="form-group dis2">
                                                        <label class="col-md-4 control-label">Interest %</label>
                                                        <div class="col-md-8">
                                                            <input type="number" class="form-control" required min="0" name="interest%" id="interestp" value="<?php echo $interest; ?>" placeholder="Interest %" step="any">
                                                        </div>
                                                    </div>
                                                    <?php } ?>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Opening Date</label>
                                                        <div class="col-md-8">
                                                            <input type="date" class="form-control" name="open_date" id="open_date" value="<?php echo $opndate; ?>" placeholder="Enter Opening Date">
                                                        </div>
                                                    </div>

                                                    <?php
                                                        if ($type == 'daily') { 
                                                    ?>
                                                    <div class="form-group dis1">
                                                        <label class="col-md-4 control-label">Loan Period</label>
                                                        <div class="col-md-8">
                                                            <input type="number" readonly class="form-control" name="loan_period" id="loan_period" value="<?php echo $loan_period; ?>" placeholder="Loan Period">
                                                        </div>
                                                    </div>

                                                    <div class="form-group dis1">
                                                        <label class="col-md-4 control-label">Extend Loan Period</label>
                                                        <div class="col-md-8">
                                                            <input type="number" class="form-control" name="extending_days" id="extending_days" placeholder="Enter Days">
                                                        </div>
                                                    </div>

                                                    <div class="form-group dis1">
                                                        <label class="col-md-4 control-label">Total Extended Days</label>
                                                        <div class="col-md-8">
                                                            <input type="hidden" class="form-control" readonly name="dext_duration" id="dext_duration" value="<?php echo $ext_duration; ?>" placeholder="0">
                                                            <input type="number" class="form-control" readonly name="ext_duration" id="ext_duration" value="<?php echo $ext_duration; ?>" placeholder="0">
                                                        </div>
                                                    </div>
                                                    <?php } ?>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Closing Date</label>
                                                        <div class="col-md-8">
                                                            <input type="hidden" class="form-control" name="dclose_date" id="dclose_date" value="<?php echo $clodate; ?>"placeholder="Enter ClosingTax Date">
                                                            <input type="date" class="form-control" name="close_date" id="close_date" value="<?php echo $clodate; ?>"placeholder="Enter Closing Date">
                                                        </div>
                                                    </div>

                                                    <div class="form-group display-hide">
                                                        <label class="col-md-4 control-label">Total Amount</label>
                                                        <div class="col-md-8">
                                                            <input type="hidden" class="form-control" name="total_amount" id="total_amount" value="<?php echo $total_amount; ?>" placeholder="Total Amount">
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <h3><center>Security Person Details</center></h3>
                                                    <br><br><br>
                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Name</label>
                                                        <div class="col-md-6">
                                                            <input type="text" class="form-control" name="s_name" id="s_name" value="<?php echo $s_name; ?>" placeholder="Enter Name" >
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Address</label>
                                                        <div class="col-md-6">
                                                            <textarea rows="3" cols="10" class="form-control" id="s_address" name="s_address" placeholder="Enter Address"><?php echo $s_address; ?></textarea>
                                                        </div>
                                                    </div>


                                                    <div class="form-group">
                                                        <label class="col-md-4 control-label">Phone</label>
                                                        <div class="col-md-6">
                                                            <input type="tel" pattern="[0-9]{7,15}" class="form-control" name="s_phone" id="s_phone" value="<?php echo $s_phone; ?>" placeholder="Enter Phone Number">
                                                        </div>
                                                    </div>                                  

                                                </div>
                                                </div>
                                                <div class="form-actions" style="border-top:0 !important;">
                                                    <div class="row">
                                                        <div class="col-md-offset-4 col-md-9">
                                                            <button type="submit" name="submit" class="btn btn-circle green">Submit</button>
                                                            <a href="cstmrlist.php"><button type="button" class="btn btn-circle grey-salsa btn-outline">Cancel</button></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- END FORM-->
                                            <?php
                                            if(isset($_POST["submit"])){
                                                $e_amount = $_POST['debit_amount'] - $amount;
                                                $debit_amount = $_POST['debit_amount'];
                                                $open_date = $_POST['open_date'];

                                                if ($_POST['payment_type'] == 'daily') {
                                                    $interest = $_POST['interest'];
                                                    $total_amount = $interest + $debit_amount;
                                                $query = "UPDATE clients SET name ='".$_POST['name']."',registration_no ='".$_POST['registration_no']."',debit_amount ='".$_POST['debit_amount']."',debit_loan ='".$_POST['debit_amount']."',payment_type ='".$_POST['payment_type']."',address ='".$_POST['address']."',place ='".$_POST['place']."',landphone ='".$_POST['landphone']."',mobile ='".$_POST['mobile']."',pin ='".$_POST['pin']."',description ='".$_POST['description']."',open_date ='".$_POST['open_date']."',close_date ='".$_POST['close_date']."',interest ='".$_POST['interest']."',total_amount ='$total_amount',duration ='".$_POST['loan_period']."',extended_duration ='".$_POST['ext_duration']."' WHERE registration_no = $id";
                                                $s_query = "UPDATE security_person SET name ='".$_POST['s_name']."',address ='".$_POST['s_address']."',phone ='".$_POST['s_phone']."' WHERE client_id = $id";
                                                mysqli_query($con,$s_query);
                                                $insert = mysqli_query($con,$query);
                                                $sql0 = mysqli_query($con,"SELECT * FROM payment_details WHERE customer_regno = '$id'");
                                                $count = mysqli_num_rows($sql0);
                                                    if ($count != 0){

                                                        $sql2 = mysqli_query($con,"SELECT id,balance_amount FROM payment_details WHERE customer_regno = '1212' ORDER BY id DESC LIMIT 1");
                                                        $debit_loans = mysqli_fetch_assoc($sql2);
                                                        $sid = $debit_loans['id'];
                                                        $dl_amt = $debit_loans['balance_amount'];
                                                        $ndl = $dl_amt + $e_amount;

                                                        $query3= "UPDATE payment_details SET balance_amount = '$ndl' WHERE id = $sid";
                                                        mysqli_query($con,$query3);
                                                    }
                                                }else{
                                                    $mcdate = $open_date;
                                                         // date("Y-m-d",strtotime(date("Y-m-d",strtotime($_POST['open_date'])). " +2 month"));
                                                    $mddate = date("Y-m-d",strtotime(date("Y-m-d",strtotime($mcdate)). " +1 month"));
                                                    $intr = $_POST['interest%'];
                                                    $debit_amount = $_POST['debit_amount'];
                                                    $intr_amount = ($debit_amount * $intr)/100;
                                                    $sql3 = mysqli_query($con,"SELECT idate,cdate FROM minterest_details WHERE registration_no = '$id'");
                                                    $dates = mysqli_fetch_assoc($sql3);
                                                    $idate = $dates['idate'];
                                                    $cdate = $dates['cdate'];
                                                    if ($idate == $cdate) {
                                                        $query2 = "UPDATE minterest_details SET cdate ='$mcdate',interest ='$intr',interest_amount ='$intr_amount',balinterest_amount ='$intr_amount',due_date ='$mddate' WHERE registration_no = $id";
                                                        mysqli_query($con,$query2);
                                                    }else{
                                                        $query2 = "UPDATE minterest_details SET interest ='$intr',interest_amount ='$intr_amount',balinterest_amount ='$intr_amount' WHERE registration_no = $id";
                                                        mysqli_query($con,$query2);
                                                    }
                                                    $sql0 = mysqli_query($con,"SELECT * FROM payment_details WHERE customer_regno = '$id' AND collected_amount != 0");
                                                    $count = mysqli_num_rows($sql0);
                                                    if ($count == 0){
                                                        $query3= "UPDATE payment_details SET balance_amount = '".$_POST['debit_amount']."' WHERE customer_regno = $id";
                                                        mysqli_query($con,$query3);
                                                        $query = "UPDATE clients SET name ='".$_POST['name']."',registration_no ='".$_POST['registration_no']."',debit_amount ='".$_POST['debit_amount']."',debit_loan ='".$_POST['debit_amount']."',payment_type ='".$_POST['payment_type']."',address ='".$_POST['address']."',place ='".$_POST['place']."',landphone ='".$_POST['landphone']."',mobile ='".$_POST['mobile']."',pin ='".$_POST['pin']."',description ='".$_POST['description']."',open_date ='".$_POST['open_date']."',close_date ='".$_POST['close_date']."',interest ='$intr',total_amount ='".$_POST['debit_amount']."',duration ='".$_POST['loan_period']."',extended_duration ='".$_POST['ext_duration']."' WHERE registration_no = $id";
                                                        $s_query = "UPDATE security_person SET name ='".$_POST['s_name']."',address ='".$_POST['s_address']."',phone ='".$_POST['s_phone']."' WHERE client_id = $id";
                                                        mysqli_query($con,$s_query);
                                                        $insert = mysqli_query($con,$query);
                                                        // $mcdate = date("Y-m-d",strtotime(date("Y-m-d",strtotime($_POST['open_date'])). " +2 month"));
                                                        // $mddate = date("Y-m-d",strtotime(date("Y-m-d",strtotime($mcdate)). " +1 month"));
                                                        // $intr = $_POST['interest%'];
                                                        // $damt = $_POST['debit_amount'];
                                                        // $intr_amount = ($debit_amount * $intr)/100;
                                                        // $query2 = "UPDATE minterest_details SET cdate ='$mcdate',registration_no ='".$_POST['registration_no']."',interest ='$intr',interest_amount ='$intr_amount',balinterest_amount ='$intr_amount',due_date ='$mddate' WHERE registration_no = $id";
                                                        //     mysqli_query($con,$query2);
                                                    }else{
                                                        $sql2 = mysqli_query($con,"SELECT debit_loan FROM clients WHERE registration_no = '$id' ");
                                                        $debit_loans = mysqli_fetch_assoc($sql2);
                                                        $dl_amt = $debit_loans['debit_loan'];
                                                        $ndl = $dl_amt + $e_amount;
                                                        $query = "UPDATE clients SET name ='".$_POST['name']."',registration_no ='".$_POST['registration_no']."',debit_amount ='".$_POST['debit_amount']."',debit_loan ='$ndl',payment_type ='".$_POST['payment_type']."',address ='".$_POST['address']."',place ='".$_POST['place']."',landphone ='".$_POST['landphone']."',mobile ='".$_POST['mobile']."',pin ='".$_POST['pin']."',description ='".$_POST['description']."',open_date ='".$_POST['open_date']."',close_date ='".$_POST['close_date']."',interest ='$intr',total_amount ='".$_POST['debit_amount']."',duration ='".$_POST['loan_period']."',extended_duration ='".$_POST['ext_duration']."' WHERE registration_no = $id";
                                                        $s_query = "UPDATE security_person SET name ='".$_POST['s_name']."',address ='".$_POST['s_address']."',phone ='".$_POST['s_phone']."' WHERE client_id = $id";
                                                            mysqli_query($con,$s_query);
                                                            $insert = mysqli_query($con,$query);
                                                    }
                                                }

                                                // if($type == 'daily'){
                                                // $query1 = "UPDATE interest_details SET registration_no ='".$_POST['registration_no']."',interest_amount ='".$_POST['interest']."' WHERE registration_no = $id";
                                                //     mysqli_query($con,$query1);
                                                // }
                                                // if($type == 'monthly'){
                                                // $sql0 = mysqli_query($con,"SELECT * FROM payment_details WHERE registration_no = '$id' AND collected_amount != 0");
                                                // $count = mysqli_num_rows($sql0);
                                                //     if ($count == 0){
                                                //         $mcdate = date("Y-m-d",strtotime(date("Y-m-d",strtotime($_POST['open_date'])). " +2 month"));
                                                //         $mddate = date("Y-m-d",strtotime(date("Y-m-d",strtotime($mcdate)). " +1 month"));
                                                //         $intr = $_POST['interest%'];
                                                //         $damt = $_POST['debit_amount'];
                                                //         $intr_amount = ($debit_amount * $intr)/100;
                                                //         $query2 = "UPDATE minterest_details SET cdate ='$mcdate',registration_no ='".$_POST['registration_no']."',interest ='$intr',interest_amount ='$intr_amount',balinterest_amount ='$intr_amount',due_date ='$mddate' WHERE registration_no = $id";
                                                //             mysqli_query($con,$query2);
                                                //         }
                                                        // else{
                                                        //     while ($paydtl = mysqli_fetch_array($sql0)){
                                                        //     $balance_amount = 0;
                                                        //     $balance_amount = $paydtl['balance_amount'];
                                                        //         }
                                                        //     $mcdate = date("Y-m-d",strtotime(date("Y-m-d",strtotime($_POST['open_date'])). " +2 month"));
                                                        // $mddate = date("Y-m-d",strtotime(date("Y-m-d",strtotime($mcdate)). " +1 month"));
                                                        // $intr = $_POST['interest%'];
                                                        // $intr_amount = ($balance_amount * $intr)/100;
                                                        // $query2 = "UPDATE minterest_details SET cdate ='$mcdate',registration_no ='".$_POST['registration_no']."',interest ='$intr',interest_amount ='$intr_amount',balinterest_amount ='$intr_amount',due_date ='$mddate' WHERE registration_no = $id";
                                                        //     mysqli_query($con,$query2);
                                                        // }
                                                
                                                // if($type == 'monthly'){
                                                //     $query2 = "UPDATE payment_details SET customer_regno = '".$_POST['registration_no']."',balance_amount = '".$_POST['debit_amount']."' WHERE customer_regno = $id";
                                                //     mysqli_query($con,$query2);
                                                // }
                                                if($insert == 1){
                                                    echo "<script>alert('Client Details Succeffully Updated')</script>";
                                                    echo "<script>window.location='cstmrlist.php'</script>";
                                                }
                                                else{
                                                    echo "<script>alert('You have some form errors. Please check below'.mysqli_error($insert);)</script>";
                                                    header("Location: cstmrlist.php"); 
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>   
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>                
            </div>
        </div>
    </div>
<!-- END PAGE CONTENT INNER -->
</div>
<!-- END PAGE CONTENT BODY -->
<?php include('footer.php') ?>
<!-- <script type="text/javascript">
    $(document).ready(function(){
    if (type) {}
});
</script> -->