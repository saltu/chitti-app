CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `Account` float(50,2) DEFAULT NULL,
  `debit` float(50,2) DEFAULT NULL,
  `credit` float(50,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1; 

CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1; 

CREATE TABLE `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registration_no` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `place` varchar(100) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `pin` int(20) DEFAULT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `landphone` varchar(15) DEFAULT NULL,
  `payment_type` char(20) DEFAULT NULL,
  `debit_amount` int(50) DEFAULT NULL,
  `debit_loan` float(10,2) DEFAULT NULL,
  `open_date` date DEFAULT NULL,
  `close_date` date DEFAULT NULL,
  `interest` float(10,2) DEFAULT NULL,
  `total_amount` float(50,2) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `extended_duration` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `registration_no` (`registration_no`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1; 

CREATE TABLE `interest_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `registration_no` varchar(100) DEFAULT NULL,
  `interest_amount` float(50,2) DEFAULT NULL,
  `cdate` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1; 

CREATE TABLE `minterest_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idate` date DEFAULT NULL,
  `cdate` date DEFAULT NULL,
  `registration_no` varchar(50) DEFAULT NULL,
  `interest` float(10,2) DEFAULT NULL,
  `interest_amount` float(50,2) DEFAULT NULL,
  `balinterest_amount` float(50,2) DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4; 

CREATE TABLE `payment_details` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `pdate` date DEFAULT NULL,
  `customer_regno` int(20) DEFAULT NULL,
  `collected_amount` float(50,2) DEFAULT NULL,
  `interest_amount` float(50,2) DEFAULT NULL,
  `loan_amount` float(20,2) DEFAULT NULL,
  `balance_amount` float(20,2) DEFAULT NULL,
  `mcheck_status` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1; 

CREATE TABLE `security_person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` varchar(255) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4; 
 
 


INSERT INTO `admin` ( `id`, `name`, `username`, `password`) VALUES 
('1', 'admin', 'admin', 'admin');  


 


INSERT INTO `interest_details` ( `id`, `registration_no`, `interest_amount`, `cdate`) VALUES 
('20', '12351', '50.00', '2019-11-06');  


INSERT INTO `minterest_details` ( `id`, `idate`, `cdate`, `registration_no`, `interest`, `interest_amount`, `balinterest_amount`, `due_date`) VALUES 
('5', '2019-11-06', '2019-11-06', '12351', '2.00', '60.00', '10.00', '2019-12-06');  


 


INSERT INTO `security_person` ( `id`, `client_id`, `name`, `address`, `phone`) VALUES 
('1', '1111', 'Akash Sanjeev', 'Akku Bhavanam Haripad', '9446858804'), 
('7', '12351', '', '', ''); 