<?php include('header.php') ;
include("dbfiles/dbcon.php");
$month = date('m');
$day = date('d');
$year = date('Y');
$today = $year . '-' . $month . '-' . $day;
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <!-- BEGIN PAGE HEAD-->
    <div class="page-head">
        <div class="container">
            <!-- BEGIN PAGE TITLE -->
            <div class="page-title">
                <h1>Add Customer</h1>
            </div>
            <!-- END PAGE TITLE -->
        </div>
        <!-- END PAGE TOOLBAR -->
    </div>
</div>
<!-- END PAGE HEAD-->
<!-- BEGIN PAGE CONTENT BODY -->
<div class="page-content">
    <div class="container">
        <!-- BEGIN PAGE BREADCRUMBS -->
        <ul class="page-breadcrumb breadcrumb">
            <li>
                <a href="home.php">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Customer</span>
            </li>
        </ul>
        <!-- END PAGE BREADCRUMBS -->
        <!-- BEGIN PAGE CONTENT INNER -->
        <div class="page-content-inner">
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="icon-settings font-red"></i>
                                <span class="caption-subject font-red sbold uppercase">Customer Details</span>
                            </div>
                            <div class="tools">
                                <a href="javascript:;" class="collapse"> </a>
                                <a href="javascript:;" class="reload"> </a>
                            </div>
                        </div>
                        <div class="row container">
                            <div class="col-md-6">
                                <h1><center>Daily Customers</center></h1>
                            </div>
                            <div class="col-md-6">
                                <h1><center>Monthly Customers</center></h1>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="table-toolbar" style="margin-bottom: 80px !important;">
                                <div class="row">                                    
                                    <div class="alert alert-danger display-hide" id="alert">
                                        <button class="close" data-close="alert"></button> You have some form errors. Please check below. 
                                    </div>
                                    <div class="alert alert-success display-hide" id="alert2">
                                        <button class="close" data-close="alert"></button> Client Data is successfully Saved!
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-md-6">
                            <table class="table table-striped table-bordered table-hover" id="sample_1">
                                 <thead>
                                    <tr>
                                        <th class ="col-md-1 text-center"> Sl.No </th>
                                        <th class ="col-md-4 text-center"> Reg No </th>
                                        <th class ="col-md-4 text-center"> Name </th> 
                                        <th class ="col-md-1 text-center"> cllctn </th>
                                        <th class ="col-md-1 text-center"> Report </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; 
                                    $sql = mysqli_query($con,"SELECT * FROM clients WHERE payment_type = 'daily'");
                                    while ($result = mysqli_fetch_array($sql)) {
                                    ?>
                                            <tr id="<?php $result[0]; ?>">
                                                <td class="text-center"> <?php echo $i; ?> </td>
                                                <td class="text-center"> <?php echo $result[1]; ?> </td>
                                                <td class="text-center"> <?php echo $result[2]; ?> </td>
                                                <td class="text-center"><a class="editveh" href="amt_daily_update.php?regno=<?php echo $result[1];?>"> <i class="fa fa-inr" aria-hidden="true"></i> </a></td>
                                                <td class="text-center"><a class="deleteveh" href="customer_reportlist.php?rregno=<?php echo $result[1];?>&custname=<?php echo $result[2];?>"> <i class="fa fa-bars" aria-hidden="true"></i> </a></td>
                                            </tr>
                                    <?php
                                        $i++;
                                        }
                                    ?>
                                </tbody>
                            </table>
                            </div>
                            <div class="col-md-6">
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                 <thead>
                                    <tr>
                                        <th class ="col-md-1 text-center"> Sl.No </th>
                                        <th class ="col-md-4 text-center"> Reg No </th>
                                        <th class ="col-md-4 text-center"> Name </th> 
                                        <th class ="col-md-1 text-center"> cllctn </th>
                                        <th class ="col-md-1 text-center"> Report </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; 
                                    $sql = mysqli_query($con,"SELECT * FROM clients WHERE payment_type = 'monthly'");
                                    while ($result = mysqli_fetch_array($sql)) {
                                    ?>
                                            <tr id="<?php $result[0]; ?>">
                                                <td class="text-center"> <?php echo $i; ?> </td>
                                                <td class="text-center"> <?php echo $result[1]; ?> </td>
                                                <td class="text-center"> <?php echo $result[2]; ?> </td>
                                                <td class="text-center"><a class="editveh" href="amt_daily_update.php?regno=<?php echo $result[1];?>"> <i class="fa fa-inr" aria-hidden="true"></i> </a></td>
                                                <td class="text-center"><a class="deleteveh" href="customer_reportlist.php?rregno=<?php echo $result[1];?>&custname=<?php echo $result[2];?>"> <i class="fa fa-bars" aria-hidden="true"></i> </a></td>
                                            </tr>
                                    <?php
                                        $i++;
                                        }
                                    ?>
                                </tbody>
                            </table>
                            </div>
                            </div>                             
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>                
            </div>
        </div>
    </div>
<!-- END PAGE CONTENT INNER -->
</div>
<!-- END PAGE CONTENT BODY -->
<?php include('footer.php') ?>