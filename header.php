<!DOCTYPE html>
<html>
<head>
    <title>Chitti</title>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    
    <!-- Styles -->
    <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/layouts/layout3/css/themes/default.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="assets/layouts/layout3/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link href="assets/global/plugins/typeahead/typeahead.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="assets/pages/css/jquery-ui.css">
    <link href="assets/pages/css/invoice-2.min.css" rel="stylesheet" type="text/css" />

    <link href="assets/css/custom.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="assets/css/popup.css">

    <a href="javascript:;" class="menu-toggler"></a>
     <!-- END RESPONSIVE MENU TOGGLER -->
     <script src="assets/pages/scripts/form-input-mask.js" type="text/javascript"></script>
</head>
<body class="page-container-bg-solid page-header-menu-fixed">
    <?php session_start();
    if(empty($_SESSION['username'])){
        $_SESSION['message'] = 'Unauthorized! Please';
        header("Location:index.php");
    }
    ?>
    <!-- BEGIN HEADER -->
    <div class="page-header" >
        <!-- BEGIN HEADER TOP -->
        <div class="page-header-top">
            <div class="container">                
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler pull-left"></a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="home.php">
                        <img src="assets/images/logo.png" style="width: 200px;" alt="logo" class="logo-default">
                    </a>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">                            
                        <!-- BEGIN USER LOGIN DROPDOWN -->
                        <li class="dropdown dropdown-user dropdown-dark" style="">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                <img alt="" class="img-circle" src="assets/layouts/layout3/img/avatar.png">
                                <span class="username username-hide-mobile">
                                    <?php echo $_SESSION['username']; ?>
                                </span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <!-- <li><a href="{{ route('customer') }}"><i class="fa fa-user"></i> Customer </a></li>
                                <li><a href="{{ route('purchaser') }}"><i class="fa fa-user"></i> Purchaser </a></li>
                                <li class="divider"> </li>   -->                              
                               <!--  <li><a href="{{ route('backup') }}"><i class="fa fa-external-link-square"></i> Backup </a></li> -->
                                <li><a href="dbfiles/logout.php"><i class="icon-key"></i> Log Out </a></li>
                            </ul>
                        </li>
                        <!-- END USER LOGIN DROPDOWN -->
                    </ul>
                </div>
                <!-- END TOP NAVIGATION MENU -->
            </div>
        </div>
        <!-- END HEADER TOP -->
        <!-- BEGIN HEADER MENU -->
        <div class="page-header-menu">
            <div class="container">
                <!-- BEGIN MEGA MENU -->
                <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                <div class="hor-menu">
                    <ul class="nav navbar-nav">
                        <li class=""><a href="home.php"> Dashboard</a></li>  
                        
                        <li class="menu-dropdown classic-menu-dropdown ">
                            <a href="customer_management.php">Customer Management
                                <span class="arrow"></span>
                            </a>
                            <ul class="dropdown-menu pull-left">
                                <li class=" ">
                                    <a href="cstmrlist.php" class="nav-link  "> Add Customer </a>
                                </li>
                                <!-- <li class=" ">
                                    <a href="" class="nav-link  "> Manage customer </a>
                                </li> -->
                            </ul>
                        </li>   

                        <li class="menu-dropdown classic-menu-dropdown ">
                            <a href="daily_amt.php">Collection Entry
                                <span class="arrow"></span>
                            </a>
                            <!-- <ul class="dropdown-menu pull-left">
                                <li class=" ">
                                    <a href="daily_amt.php" class="nav-link  ">Daily Collection </a>
                                </li>
                               <li class=" ">
                                    <a href="monthly_amt.php" class="nav-link  ">Monthly Collection </a>
                                </li>
                            </ul> -->
                        </li>




                        </li>

                        <li class="menu-dropdown classic-menu-dropdown ">
                            <a href="javascript:;"> Report
                                <span class="arrow"></span>
                            </a>
                            <ul class="dropdown-menu pull-left">
                                <li class=" ">
                                    <a href="report.php" class="nav-link  "> Daily Report </a>
                                </li>
                                <li class=" ">
                                    <a href="customer_report.php" class="nav-link  "> Customer Report  </a>
                                </li>
                            </ul>
                        </li>
                       
                    </ul>
                </div>
                <!-- END MEGA MENU -->
            </div>
        </div>
        <!-- END HEADER MENU -->   
    </div>