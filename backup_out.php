<?php 
ini_set('display_errors',1); 
error_reporting(E_ALL);

class ConnectionDB {

	private $exported_database;

    public function backup() {           
        $path = "D:/db_backup/";

        $conn = mysqli_connect("localhost","root","");
		$condb = mysqli_select_db($conn, "chittiapp");
		if(!$conn || !$condb) {
			echo mysqli_error($conn);
		}
        else {
            $this->backupdb($conn);
            if($this->save($conn,$path)) {
            	foreach ($this->tables($conn) as $key => $table) {
					$tbl_query = mysqli_query($conn,"DROP table ".$table) or die(mysqli_error($con));
				}
                echo '<script type="text/javascript">alert("Backup Saved Successfully");</script>';
                echo '<script>location.href="index.php"</script>';
	        }
            else {
                echo("Error in file path");
            }
        }
    }    
    	
	function backupdb($conn) {

		$table_sql = array();

		foreach ($this->tables($conn) as $key => $table) {
			$tbl_query = mysqli_query($conn,"SHOW CREATE TABLE ".$table);
			$row2 = mysqli_fetch_row($tbl_query);
			$table_sql[] = $row2[1];
		}
		
		$solid_tablecreate_sql = implode("; \n\n" , $table_sql);

		$all_table_data = array();
		foreach ($this->tables($conn) as $key => $table) {
			$show_field = $this->view_fields($conn,$table);
			$solid_field_name = implode(", ",$show_field);
			$create_field_sql = "INSERT INTO `$table` ( ".$solid_field_name.") VALUES \n";

			//Start checking data available
			mysqli_query($conn,"SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");
			$table_data = mysqli_query($conn,"SELECT*FROM ".$table);
			if(!$table_data){
				echo 'Could not run query: '. mysql_error();
			}
			
			if (mysqli_num_rows($table_data) > 0) {
				$data_viewig = $this->view_data($conn,$table);
				$splice_data = array_chunk($data_viewig,50);
				foreach($splice_data as $each_datas){
					$solid_data_viewig = implode(", \n",$each_datas)."; ";
					$all_table_data[] = $create_field_sql.$solid_data_viewig;
				}
			
			}
			else {
				$all_table_data[]=null;
			}	
		}
		$entiar_table_data = implode(" \n\n\n",$all_table_data);
		$this->exported_database = $solid_tablecreate_sql."; \n \n".$entiar_table_data;
		return $this;
	}
    
    function tables($conn){
		$tb_name = mysqli_query($conn,"SHOW TABLES");
		$tables = array();
		while ($tb = mysqli_fetch_row($tb_name)) {
			$tables[] = $tb[0] ;
		}
		return $tables;
    }
    function view_fields($conn,$tablename){
		$all_fields = array();
		$fields = mysqli_query($conn,"SHOW COLUMNS FROM ".$tablename);
		if (!$fields) {
		 echo 'Could not run query: ' . mysql_error();
		}
		
		if (mysqli_num_rows($fields) > 0) {
			while ($field = mysqli_fetch_assoc($fields)) {
				$all_fields[] = "`".$field["Field"]."`";
			}
		}
		return $all_fields;
	}
	function view_data($conn,$tablename){
		$all_data = array();
		$table_data = mysqli_query($conn,"SELECT*FROM ".$tablename);
		if(!$table_data) {
			echo 'Could not run query: '. mysql_error();
		}

		if(mysqli_num_rows($table_data) > 0){

			
			while ($t_data = mysqli_fetch_row($table_data)) {

				$per_data = array();
				foreach ($t_data as $key => $tb_data) {
					$per_data[] = "'".str_replace("'","\'",$tb_data)."'";
				}
				$solid_data = "(".implode(", ",$per_data).")";
				$all_data[] = $solid_data;
			}
		}
		
		return $all_data;
	}
    function save($conn,$path,$name = ""){
		$name = ($name != "") ? $name : 'chittiapp';
		
		//Save file
		$file = fopen($path.$name.".sql","w+");
		$fw = fwrite($file, $this->exported_database);		
		
		if(!$fw){
			return false;
		}
		else {
			return true; 
		}
	}
}
?>

<?php

$dbConnect = new ConnectionDB();

$dbConnect->backup();
?>