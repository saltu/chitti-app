$(function(){
        $("#myBtn").on('click',function(){
            $("#myModal").css({"display":"block"});
        });
});
$(function(){
        $(".close").on('click',function(){
            $("#myModal").css({"display":"none"});
        });
});

$(function(){
        $(".close").on('click',function(){
            $("#popup").css({"display":"none"});
        });
});

// window.onclick = function(event) {
//     var modal = document.getElementById("myModal");
//   if (event.target == modal) {
//     modal.style.display = "none";
//   }
// }

$(".im").on("click",function(){
    $('#samplef').removeClass('display-hide');
    $('#sample1').addClass('display-hide');
    
});
$(".1").on("click",function(){
    $('#samplef').addClass('display-hide');
    $('#sample1').removeClass('display-hide');
});

// monthly interestvalidation
$('#intr_amt').on('input',function(){
    var entered_interest = $('#intr_amt').val();
    var cal_int_amount = $('#dint_amnt').val();
    if (parseFloat(cal_int_amount) < parseFloat(entered_interest) ) {
        $('#intr_amt').css({"border-color":"red"});
        $('#for_intval').addClass('display-hide');
        $('#for_mes').removeClass('display-hide');
    } else {        
        $('#intr_amt').css({"border-color":""});
        $('#for_intval').removeClass('display-hide');
        $('#for_mes').addClass('display-hide');
    }
});
// time period calculation
$("#extending_days").change(function(){
    var exted = $("#dext_duration").val();
    var exting_days = $("#extending_days").val();
    var tot_exted = parseInt(exted) + parseInt(exting_days);
    var c_date = new Date($("#dclose_date").val()),
        e_days = parseInt($("#extending_days").val(), 10);
    if(!isNaN(c_date.getTime())){
        c_date.setDate(c_date.getDate() + e_days);
        $("#close_date").val(c_date.toInputFormat());
        $("#ext_duration").val(tot_exted);
    } else {
            alert("Invalid Date");  
    }


});

$("#loan_period").change(function(){
    var s_date = new Date($("#open_date").val()),
        days = parseInt($("#loan_period").val(), 10);
    if(!isNaN(s_date.getTime())){
        s_date.setDate(s_date.getDate() + days);
        $("#close_date").val(s_date.toInputFormat());
    } else {
            alert("Invalid Date");  
    }
});
Date.prototype.toInputFormat = function() {
    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth()+1).toString(); // getMonth() is zero-based
    var dd  = this.getDate().toString();
    return yyyy + "-" + (mm[1]?mm:"0"+mm[0]) + "-" + (dd[1]?dd:"0"+dd[0]); // padding
};


// registration number
$("#regno").autocomplete({
    source: 'dbfiles/payment.php',
    minLength: 1,
    select: function(event, ui)
    {
      value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type:'get',
            url:'dbfiles/paymentdtls.php',
            data:{value:value},
            success:function(data)
            {
                var json = $.parseJSON(data);
                var reg = json['reg'];
                // alert(json['id']);
                $('#clname').val(json['name']);
                $('#customer_id').val(json['id']);
            }
        })
    }
});

// Client Name
$("#clname").autocomplete({
    source: 'dbfiles/clientnamefetch.php',
    minLength: 1,
    select: function(event, ui)
    {
      value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type:'get',
            url:'dbfiles/client_dtls_fetch_by_name.php',
            data:{value:value},
            success:function(data)
            {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $('#regno').val(json['regno']);
                $('#customer_id').val(json['id']);
            }
        })
    }
});


// $("#mregno").autocomplete({
//     source: 'dbfiles/mpayment.php',
//     minLength: 1,
//     select: function(event, ui)
//     {
//       value = ui.item.value;
//         // alert(ui.item.value);
//         $.ajax({
//             type:'get',
//             url:'dbfiles/mpaymentdtls.php',
//             data:{value:value},
//             success:function(data)
//             {
//                 var json = $.parseJSON(data);
//                 // alert(json['id']);
//                 $('#custname').val(json['name']);
//                 $('#customer_id').val(json['id']);
//             }
//         })
//     }
// });

// $("#mclname").autocomplete({
//     source: 'dbfiles/mclientnamefetch.php',
//     minLength: 1,
//     select: function(event, ui)
//     {
//       value = ui.item.value;
//         // alert(ui.item.value);
//         $.ajax({
//             type:'get',
//             url:'dbfiles/client_dtls_fetch_by_name.php',
//             data:{value:value},
//             success:function(data)
//             {
//                 var json = $.parseJSON(data);
//                 // alert(json['id']);
//                 $('#mregno').val(json['regno']);
//                 $('#customer_id').val(json['id']);
//             }
//         })
//     }
// });

$("#rregno").autocomplete({
    source: 'dbfiles/customerreport.php',
    minLength: 1,
    select: function(event, ui)
    {
      value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type:'get',
            url:'dbfiles/paymentdtls.php',
            data:{value:value},
            success:function(data)
            {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $('#custname').val(json['name']);
                $('#customer_id').val(json['id']);
            }
        })
    }
});

 //daily collection amount validation
$('#itemname').change(function(){
    var vqty = $('#vcpty').val();
    var mqty = $('#mqty').val();
    if (parseFloat(mqty) !=  0 || parseFloat(mqty) > 0 ) 
    {
        if(parseFloat(mqty) > parseFloat(vqty))
        {
            // alert(mqty);
            $('#item-qty').val(vqty);
        }
        else
        { 
            // alert(mqty);
            $('#item-qty').val(mqty);  
        }
    }
    if (parseFloat(mqty) < 0 || parseFloat(mqty) == 0 )
    {
            // alert(mqty);
            $('#item-qty').val(0);
    }   
});
// daily
$('#tdy_pymt').on('input',function(e){
    var amount = $('#tdy_pymt').val();
    var blnc = $('#bal_amt').val();
    var paidamount = $('#amt_paid').val();
    var debit = $('#damnt').val();
    var balance = parseFloat(blnc) - parseFloat(amount);
    // if (debit < blnc ) {
    //     var int = parseFloat(blnc) - parseFloat(debit);
    //     $('#interest').val(int);
    // }
    
    $('#dbal_amt').val(balance);

});
// monthly
$('#mtdy_pymt').on('input',function(e){
    var amount = $('#mtdy_pymt').val();
    var intr = $('#int_amnt').val();
    var total = $('#tot_amt').val();

    var bint = parseFloat(intr) - parseFloat(amount);
    var pint = $('#mtdy_pymt').val();
    if (bint <= 0) {
        var pint = 0;
        var bint = 0;
        var pint = intr;
    }
    var blnce = parseFloat(total) - parseFloat(amount);
    
    // $('#pint_amnt').val(pint);
    // $('#dint_amnt').val(bint);
    // $('#dbal_amt').val(blnce);

});

 // sales bill payment on submission

$('#amountpayed').on('input',function() {
    var salespayed = $('#amountpayed').val();
    var total = $('#item-total').val(); 
    if( parseFloat(salespayed) > parseFloat(total) ){
        $('#amountpayed').css({"border-color":"red"});
        $('#submitsalesbil').addClass('display-hide');
        $('#cancelsalesbil').addClass('display-hide');
        $('#alertsalespay').removeClass('display-hide');
    } else {        
        $('#amountpayed').css({"border-color":""});
        $('#cancelsalesbil').removeClass('display-hide');
        $('#submitsalesbil').removeClass('display-hide');
        $('#alertsalespay').addClass('display-hide');
    }

});

// purchase

//Material Name -- purchase
$(".itemnamep").autocomplete({
    source: 'purchase/product/prodname',
    minLength: 1,
    select: function(event, ui)
    {
      value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type:'get',
            url:'purchase/product/details',
            data:{value:value},
            success:function(data)
            {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $('#prodidp').val(json['id']);
                $('#mqtyp').val(json['cft']);
            }
        })
    }
});

//vehichle Number -- purchase
$(".vehiclenump").autocomplete({
    source: 'purchase/number',
    minLength: 1,
    select: function(event, ui)
    {
      value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type:'get',
            url:'purchase/number/details',
            data:{value:value},
            success:function(data)
            {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $('#vehicleidp').val(json['id']);
                $('#item-qtyp').val(json['capacity']);
                $('#vcptyp').val(json['capacity']);
            }
        })
    }
});

$('#item-ratep').on('input',function(e){
    var rate = $('#item-ratep').val();
    var qty = $('#item-qtyp').val();
    var total = $('#item-totalp').val();

    var totall = parseFloat(rate) * parseFloat(qty);

    $('#item-totalp').val(totall);

});

//purchase quantity validation
// $('#itemnamep').change(function(){
//     var vqty = $('#vcptyp').val();
//     var mqty = $('#mqtyp').val();
//     if (parseFloat(mqty) !=  0 || parseFloat(mqty) > 0 ) 
//     {
//         if(parseFloat(mqty) > parseFloat(vqty))
//         {
//             // alert(mqty);
//             $('#item-qtyp').val(vqty);
//         }
//         else
//         { 
//             // alert(mqty);
//             $('#item-qtyp').val(mqty);  
//         }
//     }
//     if (parseFloat(mqty) < 0 || parseFloat(mqty) == 0 )
//     {
//             // alert(mqty);
//             $('#item-qtyp').val(0);
//     }   
// });

$('#item-qtyp').on('input',function(e){

    var rate = $('#item-ratep').val();
    var qty = $('#item-qtyp').val();
    var total = $('#item-totalp').val();

    var totall = parseFloat(rate) * parseFloat(qty);

    $('#item-totalp').val(totall);

});

//billno -- Payment sales
$(".psbillno").autocomplete({
    source: 'psbill/no',
    minLength: 1,
    select: function(event, ui)
    {
      value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type:'get',
            url:'psbill/details',
            data:{value:value},
            success:function(data)
            {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $('#pcustid').val(json['custid']);
                $('#pcustname').val(json['custname']);
                $('#psaletot').val(json['total']);
                $('#psalespayed').val(json['amountpay'] + json['amountpayl']);
            }
        })
    }
});


//billno -- Payment Purchase
$(".ppbillno").autocomplete({
    source: 'ppbill/no',
    minLength: 1,
    select: function(event, ui)
    {
      value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type:'get',
            url:'ppbill/details',
            data:{value:value},
            success:function(data)
            {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $('#ppurid').val(json['ppurid']);
                $('#ppurname').val(json['ppurname']);
                $('#ppurchasetot').val(json['ppurchasetot']);
                $('#ppurchasepayed').val(json['amountpay'] + json['amountpayl']);
            }
        })
    }
});


//sales by cust num
$(".pcustname").autocomplete({
    source: 'customer/custname',
    minLength: 1,
    select: function(event, ui)
    {
      value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type:'get',
            url:'customer/details',
            data:{value:value},
            success:function(data)
            {
                var json = $.parseJSON(data);
                // alert(json['id']);   
                $('#psalespay').attr('readonly',false);
                $('#newsubmitsales').removeClass('display-hide');             

                $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                $.ajax({
                    type:'post',
                    url:'customer/report/list',
                    data:{value:json['id']},
                    success:function(data)
                    {
                        // var json = $.parseJSON(data);
                        // alert(data);
                        
                        $('#pcustid').val(json['id']);
                        $('#psaletot').val(json['amount']);
                        $('#psalespay').val(json['amount']);
                        $('#hide-dis').removeClass("display-hide");
                        if(json['amount'] == 0){
                            $('#psalespay').attr('readonly',true);
                            $('#newsubmitsales').addClass('display-hide');
                        }
                        $('#customerreport1').removeClass("display-hide");
                        $('.customerreport1').html(data);
                    }
                });

            }
        })
    }
});


//purchase by name
$(".ppurname").autocomplete({
    source: 'purchaser/purname',
    minLength: 1,
    select: function(event, ui)
    {
      value = ui.item.value;
        // alert(ui.item.value);
        $.ajax({
            type:'get',
            url:'purchaser/details',
            data:{value:value},
            success:function(data)
            {
                var json = $.parseJSON(data);
                // alert(json['id']);
                $('#ppurchasepay').attr('readonly',false);
                $('#newsubmitpur').removeClass('display-hide');
                
                 $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                $.ajax({
                    type:'post',
                    url:'purchaser/report/list',
                    data:{value:json['id']},
                    success:function(data)
                    {
                        // var json = $.parseJSON(data);
                        // alert(data);
                        $('#ppurid').val(json['id']);
                        $('#ppurchasetot').val(json['amount']);
                        $('#ppurchasepay').val(json['amount']);
                        
                        $('#hide-diss').removeClass("display-hide");

                        if(json['amount'] == 0){
                            $('#ppurchasepay').attr('readonly',true);
                            $('#newsubmitpur').addClass('display-hide');
                        }
                        $('#purchaserreport1').removeClass("display-hide");
                        $('.purchaserreport1').html(data);
                    }
                });

            }
        })
    }
});

//submission conditions.Sales

$('#psalespay').on('input',function() {
    var psalespay = $('#psalespay').val();
    var amountpayed = $('#psalespayed').val(); 
    var total = $('#psaletot').val(); 
    var bal = parseFloat(total) - parseFloat(amountpayed);
    if( parseFloat(psalespay) > bal){
        // $('#psalespay').attr('readonly', true);
        $('#psalespay').css({"border-color":"red"});
        $('#newsubmitsales').addClass('display-hide');
        $('#alertsalespa').removeClass('display-hide');
    } else {        
        $('#psalespay').css({"border-color":""});
        $('#newsubmitsales').removeClass('display-hide');
        $('#alertsalespa').addClass('display-hide');
    }

});

//submission conditions.purchase

$('#ppurchasepay').on('input',function() {
    var ppurchasepay = $('#ppurchasepay').val();
    var amountpayed = $('#ppurchasepayed').val(); 
    var total = $('#ppurchasetot').val(); 
    var bal = parseFloat(total) - parseFloat(amountpayed);
    if( parseFloat(ppurchasepay) > bal){
        // $('#psalespay').attr('readonly', true);
        $('#ppurchasepay').css({"border-color":"red"});
        $('#newsubmitpur').addClass('display-hide');
        $('#alertpurpa').removeClass('display-hide');
    } else {        
        $('#ppurchasepay').css({"border-color":""});
        $('#newsubmitpur').removeClass('display-hide');
        $('#alertpurpa').addClass('display-hide');
    }

});

$('#psalespay').on('input',function() {
    var psalespay = $('#psalespay').val();
    var bal = $('#psaletot').val();
    if( parseFloat(psalespay) > parseFloat(bal)){
        // $('#psalespay').attr('readonly', true);
        $('#psalespay').css({"border-color":"red"});
        $('#newsubmitsales').addClass('display-hide');
        $('#alertsalespa').removeClass('display-hide');
    } else {        
        $('#psalespay').css({"border-color":""});
        $('#newsubmitsales').removeClass('display-hide');
        $('#alertsalespa').addClass('display-hide');
    }

});

// daily/monthly

$('#ot').on('click',function(e){
    $('.dis1').addClass('display-hide');
    $('.dis2').removeClass('display-hide');
    $('.dis3').removeClass('display-hide');
    $('#interest').val('');
   
});

$('#cv').on('click',function(e){
    $('.dis1').removeClass('display-hide');
    $('.dis2').addClass('display-hide');
    $('.dis3').addClass('display-hide');
    $('#interestp').val('');

    // $('#ownername').attr('readonly',true);
    // $('#ownername').val("Chalavadakathil");   
});

// dailyreport

// $("#dailydate").datepicker({ dateFormat: "yy-mm-dd"}).datepicker("setDate", new Date());

$('#dailydate').datepicker({
    changeMonth:true,
    changeMonth:true,
    changeYear:true,
    yearRange:"1970:+0",
    dateFormat:"yy/mm/dd",
    onSelect:function(dateText){
        $('.dailyreport2').html("<p>No Data Found</p>");
        var dailydate = $('#dailydate').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
           type:'POST',
           url:'reportlist.php',
           data: { 
                dailydate: dailydate
           },
           success:function(data) {
                // alert(data);
              $('#dailyreport1').removeClass("display-hide");
              $('#dailyreport2').removeClass("display-hide");
              $('.dailyreport2').html(data);
           }
        });        
    }
});

// customreport
$('#curedatefrom').datepicker({
    changeMonth:true,
    changeMonth:true,
    changeYear:true,
    yearRange:"1970:+0",
    dateFormat:"yy/mm/dd",
    onSelect:function(dateText){
        var datefrom = $('#curedatefrom').val();
        var dateto = $('#curedateto').val();
        if(dateto == ''){
            // $('#sdatefrom').focus();
            $('#curedateto').css({"border-color":"red"});
        } else {
            $('#curedateto').css({"border-color":""});
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
               type:'POST',
               url:'customreport/data',
               data: { 
                    datefrom: datefrom,
                    dateto: dateto
               },
               success:function(data) {
                    // alert(data);
                  $('#customreport1').removeClass("display-hide");
                  $('#customreport2').removeClass("display-hide");
                  $('.customreport2').html(data);
               }
            });
        }        
    }
});
$('#curedateto').datepicker({
    changeMonth:true,
    changeMonth:true,
    changeYear:true,
    yearRange:"1970:+0",
    dateFormat:"yy/mm/dd",
    onSelect:function(dateText){
        $('#curedateto').css({"border-color":""});
        var datefrom = $('#curedatefrom').val();
        var dateto = $('#curedateto').val();
        if(datefrom == ''){
            // $('#sdatefrom').focus();
            $('#curedatefrom').css({"border-color":"red"});
            $('#curedateto').val('');
        } else {
            $('#curedatefrom').css({"border-color":""});
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
               type:'POST',
               url:'customreport/data',
               data: { 
                    datefrom: datefrom,
                    dateto: dateto
               },
               success:function(data) {
                  $('#customreport1').removeClass("display-hide");
                  $('.customreport2').html(data);
               }
            });
        }        
    }
});